import { FormControl, FormGroup } from '@angular/forms';

export class CustomValidators {

    static email(control: FormControl): { [s: string]: boolean } {

        var EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (control.value && control.value !== '' && (control.value.length <= 5 || !EMAIL_REGEXP.test(control.value))) {
            return { invalid : true };
        }
    }

    static emailRecipients(control: FormControl): { [s: string]: boolean } {
      let EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(control.value && control.value !== '') {
        let emails = control.value.split(',');
        for(let i=0; i < emails.length; i++) {
          var email = emails[i].trim();
          if (email && email !== '' && (email.length <= 5 || !EMAIL_REGEXP.test(email))) {
            return { invalid : true };
          }
        }
      }
    }

    static phoneNumber(control: FormControl): { [s: string]: boolean } {

        if (control.value && control.value !== '') {

            var numbers = control.value.match(/[0-9]+/g),
                numberValue = numbers && numbers.join('');

            if(!numberValue || numberValue.length !== 10 || (control.value.length - numberValue.length) > 4) {
                return { invalid : true };
            }
        }
    }

    static sanitizeInput(control: FormControl): { [s: string]: boolean } {

        var NO_SPECIAL_CHAR = /^[a-z\d\-_\s]+$/i;

        if (control.value && control.value !== '' && !NO_SPECIAL_CHAR.test(control.value)) {
            return { invalid : true };
        }
    }

    static matchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
      return (group: FormGroup) => {
        let passwordInput = group.controls[passwordKey];
        let passwordConfirmationInput = group.controls[passwordConfirmationKey];
        if (passwordInput.value !== passwordConfirmationInput.value) {
          return passwordConfirmationInput.setErrors({notEquivalent: true})
        }
      }
    }

    static fullName(control: FormControl): { [s: string]: boolean } {
        if (control.value &&
            control.value !== '' &&
            control.value.trim().split(' ').length < 2) {
            return { invalidName : true };
        }
    }

    static passwordPattern(control: FormControl): { [s: string]: boolean } {
      let PASSWORD_REGEXP = /^(?=.*[a-z])(?=.*\d)[A-Za-z\d$@$!%*?&]{8,}/;

        if (control.value && control.value !== '' && (control.value.length <= 5 || !PASSWORD_REGEXP.test(control.value))) {
            return { invalid : true };
        }
    }

}
