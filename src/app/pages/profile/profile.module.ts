import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilePageRoutingModule } from './profile-routing.module';

import { ProfilePage } from './profile.page';
import {AutoCompleteModule} from 'ionic4-auto-complete';
import {AutoCompleteService} from '../../services/auto-complete.service';
import {StateAutoCompleteService} from '../../services';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfilePageRoutingModule,
      AutoCompleteModule
  ],
  declarations: [ProfilePage],
    providers: [AutoCompleteService,
                 StateAutoCompleteService]
})
export class ProfilePageModule {}
