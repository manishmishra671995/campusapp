import { Component, OnInit } from '@angular/core';
import {HttpService, HelperService} from '../../services/index';
import {Events, LoadingController} from '@ionic/angular';
import { Messages } from '../../constants/constants';
import { Router } from '@angular/router';
@Component({
  selector: 'nearby',
  templateUrl: './nearby.page.html',
  styleUrls: ['./nearby.page.scss'],
})
export class NearbyPage implements OnInit {
	private loader: any;
	eventsList: any;
  nearbyActive: boolean = true;
  newMessage:any;
  	constructor(private httpService: HttpService,
    			private loading: LoadingController,
          private router: Router,
          private events: Events,
    			private helperService: HelperService,) { }

	ngOnInit() {
    this.newMessage = localStorage.getItem('campus-app-chat-message');
		this.getEvents();
    this.events.publish('feedbackPopup');
	}
 
    async getEvents() {
  		this.loader = await this.loading.create({
            message: 'Please wait...'
        });

        await this.loader.present();
  		this.httpService.get('events/get')
            .subscribe(
                (data: any) => {
                    console.log(data.data);
                    this.eventsList = [];
                    for(let i in data.data){
                      this.eventsList.push(data.data[i]);
                    }
                    this.loader.dismiss();
                },
                (err: any) => {
                    this.loader.dismiss();
                    throw(err);
                });
  	}

  	interestForEvent(interest, event_id) {
  		this.httpService.post('event/attending', {'event_id': event_id, 'is_going': interest})
            .subscribe(
                (data: any) => {
                    this.helperService.showToast(data.message, 2000, 'bottom');
                },
                (err: any) => {
                    throw(err);
                });
  	}

  	scrollToNext(number) {
  		var scrollToEventNumber = number + 1;
  		var eventName = 'event' + scrollToEventNumber;
  		let b = document.getElementById(eventName);
		 if (b) b.scrollIntoView({ behavior: "smooth" })
  	}

    scrollToPrev(number) {
    	var scrollToEventNumber = number - 1;
    	var eventName = 'event' + scrollToEventNumber;
  		let b = document.getElementById(eventName);
		 if (b) b.scrollIntoView({ behavior: "smooth" })
  	}

    searchuniversity() {
      this.router.navigateByUrl('/homesearch');
    }

    redirectToPage(page) {
      this.router.navigateByUrl('/'+page);
    }

    redirectToNearbyAppartents() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
        }
    }
    openUniversityPage() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
        }
    }

  openChat() {
    this.router.navigateByUrl('/chatlist');
  }

  checkIfTodaysDate(date) {
    let eventDate = new Date(date);
    let todaysDate = new Date();
    if(todaysDate.toDateString() === eventDate.toDateString()) {
      return 'Today';
    } else {
      return '';
    }
  }

}
