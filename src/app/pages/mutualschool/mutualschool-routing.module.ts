import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MutualschoolPage } from './mutualschool.page';

const routes: Routes = [
  {
    path: '',
    component: MutualschoolPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MutualschoolPageRoutingModule {}
