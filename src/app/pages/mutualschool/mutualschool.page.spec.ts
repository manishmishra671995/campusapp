import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MutualschoolPage } from './mutualschool.page';

describe('MutualschoolPage', () => {
  let component: MutualschoolPage;
  let fixture: ComponentFixture<MutualschoolPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MutualschoolPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MutualschoolPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
