import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MutualschoolPageRoutingModule } from './mutualschool-routing.module';

import { MutualschoolPage } from './mutualschool.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MutualschoolPageRoutingModule
  ],
  declarations: [MutualschoolPage]
})
export class MutualschoolPageModule {}
