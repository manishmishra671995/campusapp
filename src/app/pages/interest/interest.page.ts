import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {Storage} from '@ionic/storage';
import {LoadingController} from '@ionic/angular';
import { Subject } from 'rxjs/Rx';
import {Router} from '@angular/router';
import {HttpService, HelperService, UserService} from '../../services/index';
@Component({
  selector: 'interest',
  templateUrl: './interest.page.html',
  styleUrls: ['./interest.page.scss'],
})
export class InterestPage implements OnInit {
	interestsMaster:any;
	searchedInterest:any;
    private loader: any;
    searchText:any;
	savedUserData:any;
	selectedInterests = [];
	selectedInterestsName = [];
  newMessage:any;
	inputNameAndEmailListener: Subject<any> = new Subject<any>();
    constructor(private storage: Storage,
                private userService: UserService,
                private httpService: HttpService,
                private loading: LoadingController,
                private router: Router,
                private helperService: HelperService,
          			private changeRef: ChangeDetectorRef,) {
        		    storage.get('getProfile').then((val) => {
        	        this.interestsMaster = val.interest;
        	        console.log(this.interestsMaster);
    	          });
      this.savedUserData = this.userService.getSavedUserInfo();
      console.log(this.savedUserData);
  	}

  	ngOnInit() {
      this.newMessage = localStorage.getItem('campus-app-chat-message');
      console.log(this.savedUserData, this.interestsMaster)
      if(this.savedUserData && this.savedUserData.existingInterest && this.savedUserData.existingInterest.length > 0) {
        for (let i = 0 ; i < this.savedUserData.existingInterest.length; i++) {
          let interest = this.savedUserData.existingInterest[i].interest;
          if(interest) {
            this.interestSelected(interest.id, interest.interest_name);
          }
        }
      }
  		this.inputNameAndEmailListener
	      .debounceTime(500)
	      .subscribe((data)=> {
	       this.interestSearched(data.value);
	    });
  	}
  	interestSelected(interestId, interestName) {
      if(this.selectedInterests.indexOf(interestId) <= -1) {
        if(this.selectedInterestsName.indexOf('interestName') <= -1) {
          this.selectedInterestsName.push(interestName);
          this.selectedInterests.push(interestId);
        }
        
        console.log('selectedInterestsName', this.selectedInterestsName)
        this.changeRef.detectChanges();
      }

  	}

  	interestSearched(value) {
      let searchedvalue = value.toLowerCase();
  		this.searchedInterest = [];
  		for(var i = 0; i < this.interestsMaster.length; i++) {
  			if(this.interestsMaster[i].interest_name.indexOf(searchedvalue) > -1) {
  				this.searchedInterest.push(this.interestsMaster[i])
  			}
  		}
  		this.changeRef.detectChanges();

  	}
    async saveInterest() {
      console.log(this.selectedInterests)
      this.savedUserData.user_interests = this.selectedInterests;
      this.loader = await this.loading.create({
        message: 'Please wait...'
      });
      await this.loader.present();
      console.log(this.savedUserData);
      this.httpService.post('user/profile_update', this.savedUserData)
        .subscribe(
            (data: any) => {
                // TODO check if loadercal storage is available only then set token.Not sure if such a case can occur in mobile.
                // let test = data;
                console.log(data);
                this.loader.dismiss();
                this.userService.saveProfileInfo(this.savedUserData);
                this.router.navigateByUrl('/homesearch');
                // this.router.navigateByUrl('/homesearch');

            },
            (err: any) => {
                this.loader.dismiss();
                console.log(err);
                // this.helperService.showToast(errors.message, 2000, 'bottom');
            });
    }
    skipInterest() {
      this.router.navigateByUrl('/homesearch');
    }

    removeInterest(index, id) {
      console.log(id, index)
      this.selectedInterestsName.splice(index, 1);
      let selectedInterestId = this.selectedInterests.indexOf(id);
      this.selectedInterests.splice(selectedInterestId, 1)
    }

}
