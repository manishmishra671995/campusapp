import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from '../../validators/custom-validators';
import { UserService, HttpService, HelperService } from '../../services/index';
@Component({
  selector: 'createaccount2',
  templateUrl: './createaccount2.page.html',
  styleUrls: ['./createaccount2.page.scss'],
})
export class Createaccount2Page implements OnInit {
	createAccount2Form: FormGroup;
	userData;
	private loader: any;
	userDetails = {email:'', agreeBox:false};
    constructor(private navCtrl: NavController,
	  		  private userService: UserService,
	  		  private httpService: HttpService,
	  		  private helperService: HelperService,
	  		  private loading: LoadingController,
	  		  public formBuilder: FormBuilder) { }

    ngOnInit() {
	  	this.userData = this.userService.getNewUserObj();
	  	console.log(this.userData, 'userData')
	  	this.createAccount2Form = this.formBuilder.group({
	      email: [this.userDetails.email, Validators.compose([Validators.required, CustomValidators.email])],
	      agreeBox: [this.userDetails.agreeBox]
	    });
    }

    async createpassword(){
	    console.log(this.userDetails);
	    this.userData.email = this.userDetails.email;
	    this.userData.agreeBox = this.userDetails.agreeBox;
	    this.userService.saveNewUserObj(this.userData);

	    /*verify email*/
	    this.loader = await this.loading.create({
            message: 'Please wait...'
        });
        await this.loader.present();
	    this.httpService.post('auth/check_email', {'email': this.userData.email})
            .subscribe(
                (data: any) => {
                    // TODO check if loadercal storage is available only then set token.Not sure if such a case can occur in mobile.
                    // let test = data;
                    console.log(data);
                    this.loader.dismiss();
                    this.navCtrl.navigateRoot('/createpassword');
                },
                (err: any) => {
                    this.loader.dismiss();
                    console.log(err.error.message);
                    this.helperService.showToast(err.error.message, 3000, 'bottom');
                });
	    /*verify email*/

	    // this.navCtrl.navigateRoot('/createpassword');
  	}

  	goBack() {
	    this.navCtrl.back();
  	}

}
