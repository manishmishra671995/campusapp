import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApartmentdetailPage } from './apartmentdetail.page';

const routes: Routes = [
  {
    path: '',
    component: ApartmentdetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApartmentdetailPageRoutingModule {}
