import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {HttpService, HelperService} from '../../services/index';
import {Events, LoadingController, NavController} from '@ionic/angular';
import { Messages } from '../../constants/constants';
import { Router } from '@angular/router';

@Component({
  selector: 'apartmentdetail',
  templateUrl: './apartmentdetail.page.html',
  styleUrls: ['./apartmentdetail.page.scss'],
})
export class ApartmentdetailPage implements OnInit {
    appartmentId: any;
    applicationId: any;
    application_room_id: any;
  	isApplicationForRoom: boolean = false;
    appartmentDetail: any;
  	nearByApartmentsActive: boolean = true;
  	private loader: any;
    getAppartmentDetails:any=null;
    newMessage:any;
    constructor(private route: ActivatedRoute,
    			private httpService: HttpService,
    			private loading: LoadingController,
          private router: Router,
          private navCtrl: NavController,
          private changeRef: ChangeDetectorRef,
				private helperService: HelperService,) { }

  	ngOnInit() {
      this.newMessage = localStorage.getItem('campus-app-chat-message');
  		this.appartmentId = this.route.snapshot.paramMap.get('id');
      if(this.route.snapshot.paramMap.get('application_id') && this.route.snapshot.paramMap.get('roomid')) {
          this.applicationId = this.route.snapshot.paramMap.get('application_id');
          this.application_room_id = this.route.snapshot.paramMap.get('roomid');
          console.log(this.applicationId, this.application_room_id)
          this.isApplicationForRoom = true;
      }
	  	this.getAppartmentDetail(this.appartmentId);
  	}
    goBack() {
      this.navCtrl.back();
    }
    application(id) {
      this.router.navigateByUrl('application' +'/'+ this.appartmentId + '/' + id)
    }
  	async getAppartmentDetail(appartmentId) {
  		this.loader = await this.loading.create({
            message: 'Please wait...'
        });

        await this.loader.present();
  		let appartmentUrl = 'apartment/show/'+appartmentId;
  		this.httpService.get(appartmentUrl)
            .subscribe(
                (data: any) => {
                    console.log(data.data);
                    this.getAppartmentDetails = data.data;
                    console.log('appartmentdetails', this.getAppartmentDetails)
                    this.changeRef.detectChanges();
                    this.loader.dismiss();

                },
                (err: any) => {
                    this.loader.dismiss();
                    throw(err);
                });
  	}

  	getCommuteImage(commute) {
  		let commuteImageName = commute.commute_name.toLowerCase();
  		if(commuteImageName.indexOf(' ') > -1) {
  			commuteImageName = commuteImageName.split(' ')[1];
  		}
  		return '../assets/icon/'+commuteImageName+'-icon.png';

  	}

  	getNearByImage(nearby) {
  		let nearbyName = nearby.nearby_name.toLowerCase();
  		return '../assets/icon/'+nearbyName+'-icon.png';
  	}

    openInfo(url) {
      window.open(url, '_blank');
    }

  	getAmenitiesImage(amenities) {
  		let amenityName = amenities.amenities.amenities_name.toLowerCase();
  		if(amenityName.indexOf(' ') > -1) {
  			amenityName = amenityName.split(' ')[0];
  		}
      if(amenityName == 'swimming') {
        amenityName = 'pool';
      }
      if(amenityName == 'pets') {
        amenityName = 'pet';
      }
  		return '../assets/icon/'+amenityName+'-icon.png';
  	}

    searchuniversity() {
      this.router.navigateByUrl('/homesearch');
    }

    redirectToPage(page) {
      this.router.navigateByUrl('/'+page);
    }

    redirectToNearbyAppartents() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
        }
    }
    openUniversityPage() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
        }
    }
    openChat() {
      this.router.navigateByUrl('/chatlist');
    }

    reviewAppartment() {
      let url = '/universityreview/appartment/' + this.appartmentId;
        this.router.navigateByUrl(url);
    }

    roundValues(value) {
      return parseFloat(value)
    }

    async acceptRoomRequest(applicationId) {
      this.loader = await this.loading.create({
            message: 'Please wait...'
        });

        await this.loader.present();
      let applicationUrl = 'apartment/coapplicant_apply_apartment/'+applicationId;
      this.httpService.get(applicationUrl)
        .subscribe(
          (data: any) => {
              console.log(data.data);
              this.loader.dismiss();

          },
          (err: any) => {
              this.loader.dismiss();
              throw(err);
          });
    }


}
