import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ApartmentdetailPageRoutingModule } from './apartmentdetail-routing.module';

import { ApartmentdetailPage } from './apartmentdetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ApartmentdetailPageRoutingModule
  ],
  declarations: [ApartmentdetailPage]
})
export class ApartmentdetailPageModule {}
