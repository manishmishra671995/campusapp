import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { CustomValidators } from '../../validators/custom-validators';
import { HttpService, HelperService, UserService } from '../../services/index';
@Component({
  selector: 'otpverify',
  templateUrl: './otpverify.page.html',
  styleUrls: ['./otpverify.page.scss'],
})
export class OtpverifyPage implements OnInit {
	otpDetails = {'user_id': '', 'otp': ''};
	otp1: any;
  	otp2: any;
  	otp3: any;
  	otp4: any;
  	otp5: any;
  	loader: any;
  	constructor(private helperService: HelperService,
              private loading: LoadingController,
              private httpService: HttpService,
			  private navCtrl: NavController,
			  private changeRef: ChangeDetectorRef,
			  private userService: UserService) {
    }

	ngOnInit() {
	}

	 moveFirstFocus(nextElement, event, currElement) {
	    let otpVal = '';
	    if(event == '') {
	    	return;
	    }
	    otpVal = event
	    // if(event.target.value.length > 1 ) {
	    //   otpVal = event.target.value.split('');
	    // } 
	    if(otpVal.length > 5) {
	      this.helperService.showToast('Invalid otp.', 2000, 'bottom');
	      event = '';
	      this.otp1 = '';
	      currElement.value = '';
	      this.changeRef.detectChanges();
	      return;
	    }
	    if(otpVal.length > 1 && otpVal.length < 5) {
	      otpVal = event.target.value.split('')[0];
	      event.target.value = otpVal;
	    }
	    if(otpVal.length == 5) {
	      this.otp1 = otpVal[0];
	      this.otp2 = otpVal[1];
	      this.otp3 = otpVal[2];
	      this.otp4 = otpVal[3];
	      this.otp5 = otpVal[4];
	      this.changeRef.detectChanges();
	    } else {
	      if(otpVal && nextElement && otpVal != ' ') {
	        nextElement.setFocus();
	      }
	    }
	    this.checkPasswordLength();
	   //  if(this.otp1 && this.otp2 && this.otp3 && this.otp4 && this.otp5) {
		  // 	let password = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
		  // 	if(password.length == 5) {
		  //     	this.passwordEntered(password);
	   //    	}
	  	// }
	}

    moveFocus(nextElement, event) {
	    let otpVal = '';
	    if(event.target.value.length > 1 ) {
	      otpVal = event.target.value.split('');
	    } 
	    if(otpVal.length > 5) {
	      this.helperService.showToast('Invalid otp.', 2000, 'bottom');
	      event.target.value = '';
	      return;
	    }
	    if(otpVal.length > 1 && otpVal.length < 5) {
	      otpVal = event.target.value.split('')[0];
	      event.target.value = otpVal;
	    }
	    if(otpVal.length == 5) {
	      this.otp1 = otpVal[0];
	      this.otp2 = otpVal[1];
	      this.otp3 = otpVal[2];
	      this.otp4 = otpVal[3];
	      this.otp5 = otpVal[4];
	      this.changeRef.detectChanges();
	    } else {
	      if(event.keyCode != '8' && nextElement && event.target.value != ' ') {
	        nextElement.setFocus();
	      }
	    }
	    this.checkPasswordLength();
	   //  if(this.otp1 && this.otp2 && this.otp3 && this.otp4 && this.otp5) {
		  // 	let password = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
		  // 	if(password.length == 5) {
		  //     	this.passwordEntered(password);
	   //    	}
	  	// }
	}


	checkPasswordLength() {
	    setTimeout (() => {
          	if(this.otp1 && this.otp2 && this.otp3 && this.otp4 && this.otp5) {
			  	let password = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
			  	if(password.length == 5) {
			      	this.passwordEntered(password);
		      	}
	      	}
	    }, 300);
	    
  }
    goBack() {
    	this.navCtrl.back();
    }

    async passwordEntered(password) {
	    this.loader = await this.loading.create({
            message: 'Please wait...'
        });

        await this.loader.present();
	    let user = this.userService.getNewUserObj();
	    this.otpDetails.otp = password;
	    this.otpDetails.user_id = user.id;
	    this.httpService.post( 'auth/verify_otp', this.otpDetails).subscribe(
	    data => {
	      console.log(data)
	      this.loader.dismiss();
	      // this.helperService.showToast(data.message, 2000, 'bottom');
	      this.navCtrl.navigateRoot('/login');
	    },
	    err => {
	      this.loader.dismiss();
	      throw (err);
	       
	      
	    });
  	}

}
