import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdduniversityPage } from './adduniversity.page';

const routes: Routes = [
  {
    path: '',
    component: AdduniversityPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdduniversityPageRoutingModule {}
