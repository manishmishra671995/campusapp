import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AdduniversityPage } from './adduniversity.page';

describe('AdduniversityPage', () => {
  let component: AdduniversityPage;
  let fixture: ComponentFixture<AdduniversityPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdduniversityPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AdduniversityPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
