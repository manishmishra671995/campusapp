import { Component, OnInit } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { Messages } from '../../constants/constants';
import {HttpService, HelperService} from '../../services/index';
import { Router } from '@angular/router';

@Component({
  selector: 'adduniversity',
  templateUrl: './adduniversity.page.html',
  styleUrls: ['./adduniversity.page.scss'],
})
export class AdduniversityPage implements OnInit {
  newMessage:any;
  constructor(private navCtrl: NavController,
				private router: Router,
        private httpService: HttpService,
        private helperService: HelperService,
        private menu: MenuController) { }

  ngOnInit() {
    this.newMessage = localStorage.getItem('campus-app-chat-message');
  }

  searchuniversity() {
  	this.router.navigateByUrl('/homesearch');
  }

  redirectToPage(page) {
    this.router.navigateByUrl('/'+page);
  }

  goBack() {
    this.navCtrl.back();
  }

  ionViewWillEnter() {
    // this.menu.enable(false);
  }

  redirectToNearbyAppartents() {
    if(localStorage.getItem('campus-app-following-university-id')) {
        this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
    } else {
        this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
    }
  }
  openUniversityPage() {
    if(localStorage.getItem('campus-app-following-university-id')) {
        this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
    } else {
        this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
    }
  }

  openChat() {
    this.router.navigateByUrl('/chatlist');
  }

}
