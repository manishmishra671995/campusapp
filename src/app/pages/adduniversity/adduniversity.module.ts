import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdduniversityPageRoutingModule } from './adduniversity-routing.module';

import { AdduniversityPage } from './adduniversity.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdduniversityPageRoutingModule
  ],
  declarations: [AdduniversityPage]
})
export class AdduniversityPageModule {}
