import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {UserService} from '../../services';
import {ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import { Messages } from '../../constants/constants';
import {LoadingController, NavController} from '@ionic/angular';
import {HttpService, HelperService} from '../../services/index';
@Component({
    selector: 'friend-profile',
    templateUrl: './friend-profile.page.html',
    styleUrls: ['./friend-profile.page.scss'],
})
export class FriendProfilePage implements OnInit {
    genderImages = [
        {id: 0, url: '../assets/icon/na-icon.png', index: 0, name: 'n/a'},
        {id: 1, url: '../assets/icon/male-icon.png', index: 1, name: 'male'},
        {
            id: 2,
            url: '../assets/icon/female-icon.png',
            index: 2,
            name: 'female'
        }];

    selectedHabit = {id: 1, url: '../assets/icon/meat-icon.png', index: 0, text: 'meat'};

    dietImages = [{
        id: 3, url: '../assets/icon/na-icon.png', index: 2, 'habbit_option_name': 'N/A'
    },
        {
            id: 1, url: '../assets/icon/meat-icon.png', index: 0, text: 'meat', 'habbit_option_name': 'Meat'
        },
        {
            id: 2, url: '../assets/icon/nomeat-icon.png', index: 1, text: 'no meat', 'habbit_option_name': 'No-Meat'
        }];


    smokehabitImages = [{
        id: 3, url: '../assets/icon/na-icon.png', index: 2, 'habbit_option_name': 'N/A'
    },
        {
            id: 2, url: '../assets/icon/smoking-icon.png', index: 1, text: 'smoke', 'habbit_option_name': 'Smokes'
        },
        {
            id: 1, url: '../assets/icon/nosmoking-icon.png', index: 0, text: 'doesn\'t smoke', 'habbit_option_name': 'Doesnt Smoke'
        }];

    drinkingImage = [{
        id: 3, url: '../assets/icon/na-icon.png', index: 2, 'habbit_option_name': 'N/A'
    },
        {
            id: 2, url: '../assets/icon/drinks-icon.png', index: 1, text: 'drink', 'habbit_option_name': 'Drinks'
        },
        {
            id: 1, url: '../assets/icon/nodrinks-icon.png', index: 0, text: 'doesn\'t drink ', 'habbit_option_name': 'Doesnt Drink'
        }];

    sleepingImage = [{
        id: 3, url: '../assets/icon/na-icon.png', index: 2, 'habbit_option_name': 'N/A'
    },
        {
            id: 2, url: '../assets/icon/moon-icon.png', index: 1, text: 'Night Owl', 'habbit_option_name': 'Night Owl'
        },
        {
            id: 1, url: '../assets/icon/sun-icon.png', index: 0, text: 'Early Bird', 'habbit_option_name': 'Early Bird'
        }];

    selectedSmokeHabit = {id: 1, url: '../assets/icon/nosmoking-icon.png', index: 0, text: 'doesn\'t smoke'};

    user: any = {};
    profileform = {
        firstname: '',
        lastname: '',
        status: null,
        bio: ''
    };
    profileUrl: null;
    dietHabitName: null;
    smokingHabitName: null;
    drinkingHabitName: null;
    sleepingHabitName: null;

    genderIndex: number = 0;
    dietIndex: any = 0;
    smokingIndex: any = 0;
    drinkingIndex: any = 0;
    sleepingIndex: any = 0;

    dietHabitId = 0;
    smokingHabitId = 0;
    drinkingHabitId = 0;
    sleepingHabitId = 0;
    countryId: any;
    stateId: any;
    selectedCountry: any = '';
    selectedState: any = '';
    socialLinks: any = [];
    user_interests: any[] = [];
    friendId: any;
    private loader: any;
    newMessage:any;
    constructor(private userService: UserService,
                private route: ActivatedRoute,
                private router: Router,
                private httpService: HttpService,
                private navCtrl: NavController,
                private helperService: HelperService,
                private loading: LoadingController,
                private changeRef: ChangeDetectorRef
    ) {
        this.friendId = this.route.snapshot.paramMap.get('id');
        this.other_profile_show(this.friendId);


    }

    ngOnInit() {
        this.newMessage = localStorage.getItem('campus-app-chat-message');
    }


    async other_profile_show(friendId) {
        this.userService.getOther_profile_show(friendId).then((data) => {
            console.log('data===>', data);

            this.user = data;
            this.profileform.firstname = data['first_name'];
            this.profileform.lastname = data['last_name'];
            if(data['user_profile']!=null){
                this.profileform.bio = data['user_profile']['bio'];
                if(data['user_profile']['sex']!==null){
                    this.genderIndex = data['user_profile']['sex'];

                }
                this.profileform.status = JSON.stringify(data['user_profile']['status']);
                if (data['user_profile']['country'] != null) {
                    this.countryId = data['user_profile']['country']['id'];
                    this.selectedCountry = data['user_profile']['country']['name'];
                }
                if (data['user_profile']['state'] != null) {
                    this.stateId = data['user_profile']['state']['id'];
                    this.selectedState = data['user_profile']['state']['name'];
                }
            }

            if (data['user_profile_picture'] != null)
                this.profileUrl = data['user_profile_picture']['profile_picture'];



            if (data['user_interests'] != undefined)
                this.user_interests = data['user_interests'];

            if (data['user_social_links'] != undefined)
                this.socialLinks = data['user_social_links'];
            if (data['user_habbits'] != null)
                this.setHabits(data['user_habbits']);


            this.changeRef.detectChanges();

        });
    }


    async setHabits(user_habbit) {

        for (let i = 0; i < user_habbit.length; i++) {
            switch (user_habbit[i].habbit.habbit_name) {
                case 'diet': {
                    if(user_habbit[i].habbit_option!=null) {

                        this.dietIndex = this.dietImages.findIndex(x => x.habbit_option_name === user_habbit[i].habbit_option.habbit_option_name);

                        this.dietHabitId = user_habbit[i].habbit_option.id;

                        this.dietHabitName = user_habbit[i].habbit_option.habbit_option_name;
                        console.log(this.dietIndex, this.dietHabitId, this.dietHabitName);

                    }

                    break;
                }
                case 'smoking': {
                    if(user_habbit[i].habbit_option!=null) {

                        this.smokingIndex = this.smokehabitImages.findIndex(x => x.habbit_option_name === user_habbit[i].habbit_option.habbit_option_name);
                        this.smokingHabitId = user_habbit[i].habbit_option.id;
                        this.smokingHabitName = user_habbit[i].habbit_option.habbit_option_name;

                    }

                    console.log(this.smokingIndex, this.smokingHabitId);
                    break;
                }
                case 'drinking': {
                   if(user_habbit[i].habbit_option!=null){
                       this.drinkingIndex = this.drinkingImage.findIndex(x => x.habbit_option_name === user_habbit[i].habbit_option.habbit_option_name);
                       console.log('drinking',this.drinkingIndex);

                       this.drinkingHabitId = user_habbit[i].habbit_option.id;

                       this.drinkingHabitName = user_habbit[i].habbit_option.habbit_option_name;


                   }

                    console.log('drinking', this.drinkingIndex, this.drinkingHabitId);

                    break;
                }
                case 'sleeping': {
                    if(user_habbit[i].habbit_option!=null) {

                        this.sleepingIndex = this.sleepingImage.findIndex(x => x.habbit_option_name === user_habbit[i].habbit_option.habbit_option_name);
                        this.sleepingHabitId = user_habbit[i].habbit_option.id;
                        this.sleepingHabitName = user_habbit[i].habbit_option.habbit_option_name;


                        console.log(this.sleepingHabitId);
                    }
                    break;
                }

                default: {
                    //statements;
                    break;
                }
                    this.changeRef.detectChanges();


            }
        }

    }

    openInfo(url) {
        console.log('url', url);
        window.open(url, '_blank');
    }

    openfbInfo(url) {
        let id = url.split('?id=')[1];
        window.open('fb://profile/'+id, '_system');
    }

    redirectToPage(page) {
        this.router.navigateByUrl('/'+page);
    }
    pageTobeLoaded(navPageName, id) {
        // this.router.navigateByUrl(navPageName +'/'+ id);
        this.router.navigate([navPageName, id]);
    }

    openChat() {
        this.router.navigateByUrl('/chatlist');
    }
    redirectToNearbyAppartents() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
        }
    }
    openUniversityPage() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
        }
    }
    searchuniversity() {
        this.router.navigateByUrl('/homesearch');
    }

    async ignoreUser(id) {
        this.loader = await this.loading.create({
            message: 'Please wait...'
        });

      await this.loader.present();
      let url = 'user/ignore_user/' + id;
      this.httpService.get(url)
      .subscribe(
          (data: any) => {
              this.loader.dismiss();
              this.helperService.showToast(data.message, 2000, 'bottom');
          },
          (err: any) => {
              this.loader.dismiss();
              throw(err);
          });
    }
    goBack() {
      this.navCtrl.back();
    }

}
