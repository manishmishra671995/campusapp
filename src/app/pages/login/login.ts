import {Component, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NavController} from '@ionic/angular';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {CustomValidators} from '../../validators/custom-validators';
import {UserService} from '../../services/index';
import {UserOptions} from '../../interfaces/user-options';
import {HttpClient} from '@angular/common/http';
import {Keyboard} from '@ionic-native/keyboard/ngx';
import {Storage} from '@ionic/storage';


@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
    styleUrls: ['./login.scss'],
})
export class LoginPage {
    login: UserOptions = {email: '', password: ''};
    submitted = false;
    classForHeader: any;
    signInForm: FormGroup;
    password_type: string = 'password';
    userData = {email: '', password: ''};
    deviceToken:any='';
    constructor(
        private httpClient: HttpClient,
        private userService: UserService,
        private navCtrl: NavController,
        private formBuilder: FormBuilder,
        public router: Router,
        private keyboard: Keyboard,
        private storage: Storage
    ) {
        this.storage.get('deviceToken').then((val) => {
            console.log('deviceToken-->', val);
            if (val != null) {
                this.deviceToken = val;
            }
        });
    }

    // onLogin(form: NgForm) {
    //   // this.submitted = true;

    //   // if (form.valid) {
    //   //   this.userData.login(this.login.username);
    //   //   this.router.navigateByUrl('/app/tabs/schedule');
    //   // }
    //   let userData = {'email': 'sandeep@gmail.com', 'password': '12345678', 'grant_type': 'password'}
    //   this.userService.login(userData);

    //   /*if (form.valid) {
    //     this.userData.login(this.login.email);
    //     this.router.navigateByUrl('/app/tabs/schedule');
    //   }*/

    // }


    checkFocus() {
        console.log('on click');
    }

    checkFocuss() {
        console.log('remove click');
    }


    userLogin(userData) {
        console.log(userData);
            userData.grant_type = 'password';
            userData.device_type= 'android';
            userData.device_id= this.deviceToken
        this.userService.login(userData);
        // this.navCtrl.push(ProfilePage);
    }

    togglePasswordMode() {
        this.password_type = this.password_type === 'text' ? 'password' : 'text';
    }

    onSignup() {
        this.router.navigateByUrl('/signup');
    }

    ngOnInit() {
        this.signInForm = this.formBuilder.group({
            email: [this.userData.email, Validators.compose([Validators.required, CustomValidators.email])],
            password: [this.userData.password, Validators.compose([Validators.required])]
        });

        this.keyboard.disableScroll(true);
    }


    forgotpassword() {
        this.router.navigateByUrl('/forgetpassword');
    }

    addClasstoHeader() {
        //this.classForHeader = 'notopscroll';
    }

    removeClassFromHeader() {
        this.classForHeader = '';
    }

    goBack() {
        this.navCtrl.navigateRoot('/welcome');
    }
}
