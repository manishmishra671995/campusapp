import {Component, OnInit, ViewChild, ChangeDetectorRef} from '@angular/core';
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook/ngx';
import {AlertController, LoadingController, Events} from '@ionic/angular';
import {FormGroup, Validators, FormControl, FormBuilder} from '@angular/forms';
import {AutoCompleteOptions} from 'ionic4-auto-complete';
import {HttpService, HelperService, StateAutoCompleteService} from '../../services/index';
import {AutoCompleteService} from '../../services/auto-complete.service';
import {UserService} from '../../services';
import {Subject} from 'rxjs/Rx';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {Messages} from '../../constants/constants';
import {Observable, of} from 'rxjs';
import {FilterPipe} from '../../providers/filter.pipe';
import {Country} from '../../model/country.model';
import {Storage} from '@ionic/storage';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';

@Component({
    selector: 'profileother',
    templateUrl: './profileother.page.html',
    styleUrls: ['./profileother.page.scss'],
})
export class ProfileotherPage implements OnInit {
    // profileForm: FormGroup;

    //selectedGender: any = {id: 1, url: '../assets/icon/male-icon.png', index: 0};

    genderImages = [
        {id: 0, url: '../assets/icon/na-icon.png', index: 0, name: 'n/a'},
        {id: 1, url: '../assets/icon/male-icon.png', index: 1, name: 'male'},
        {
            id: 2,
            url: '../assets/icon/female-icon.png',
            index: 2,
            name: 'female'
        }];

    selectedHabit = {id: 1, url: '../assets/icon/meat-icon.png', index: 0, text: 'meat'};

    dietImages = [{id: 3, url: '../assets/icon/na-icon.png', index: 2},
        {id: 1, url: '../assets/icon/meat-icon.png', index: 0, text: 'meat'},
        {id: 2, url: '../assets/icon/nomeat-icon.png', index: 1, text: 'no meat'}];


    smokehabitImages = [{id: 3, url: '../assets/icon/na-icon.png', index: 2},
        {id: 2, url: '../assets/icon/smoking-icon.png', index: 1, text: 'smoke'},
        {id: 1, url: '../assets/icon/nosmoking-icon.png', index: 0, text: 'doesn\'t smoke'}];

    drinkingImage = [{id: 3, url: '../assets/icon/na-icon.png', index: 2},
        {id: 2, url: '../assets/icon/drinks-icon.png', index: 1, text: 'drink'},
        {id: 1, url: '../assets/icon/nodrinks-icon.png', index: 0, text: 'does not drink '}];

    sleepingImage = [{id: 3, url: '../assets/icon/na-icon.png', index: 2},
        {id: 2, url: '../assets/icon/moon-icon.png', index: 1, text: 'Night Owl'},
        {id: 1, url: '../assets/icon/sun-icon.png', index: 0, text: 'Early Bird'}];

    selectedSmokeHabit = {id: 1, url: '../assets/icon/nosmoking-icon.png', index: 0, text: 'doesn\'t smoke'};


    facebookUrl: any = null;
    profileUrl: any = null;

    selectedCountry: any = '';
    selectedState: any = '';
    states: any;
    public options: AutoCompleteOptions;
    countryFilter: Subject<any> = new Subject<any>();
    stateFilter: Subject<any> = new Subject<any>();

    countries: any[] = [];
    country: any = '';
    observable: Observable<any>;
    countryId: any;
    stateId: any;
    userData: any;
    savedUserData: any;
    habitsData: any = [] = [];
    selected_url: any = '../assets/icon/multi-color-tri-icon.png';

    dietIndex: any = 0;
    smokingIndex: any = 0;
    drinkingIndex: any = 0;
    sleepingIndex: any = 0;
    genderIndex: any = 0;

    dietHabitId = 0;
    smokingHabitId = 0;
    drinkingHabitId = 0;
    sleepingHabitId = 0;
    countryCode: any;
    stateCode: any;

    profileform = {
        firstname: '',
        lastname: '',
        status: '0',
        bio: ''
    };

    cameraOption: any;
    galaryOption: any;
    genderId: any;
    profileActive: boolean = true;
    socialArray: any [] = [];
    instagram: any = null;
    youtube: any = null;
    facebook: any = null;
    private loader: any;
    user: any = {};
    user_interests: any = [];
    user_interestsArray: any = [];
    public progress: number = 0;
    public progressGender: number = 0;
    // Interval function   // Interval function
    protected interval: any;
    protected intervalGender: any;
    countrySearch: any = '';
    stateSearch: any = '';
    countriesList: any;
    stateList: any;
    countriesListArray: any = [];
    stateListArray: any = [];
    showCuntryList:boolean=false;
    showStateList:boolean=false;
    newMessage:any;
    constructor(private fb: Facebook,
                public alertController: AlertController,
                private userService: UserService,
                private httpService: HttpService,
                private helperService: HelperService,
                public autoComplete: AutoCompleteService,
                public stateComplet: StateAutoCompleteService,
                private storage: Storage,
                private events: Events,
                private router: Router,
                private loading: LoadingController,
                private changeRef: ChangeDetectorRef,
                private camera: Camera
                // private formBuilder: FormBuilder
    ) {

        this.options = new AutoCompleteOptions();
        this.options.autocomplete = 'on';
        this.options.cancelButtonIcon = 'assets/icons/clear.svg';
        this.options.clearIcon = 'assets/icons/clear.svg';
        this.options.debounce = 750;
        this.options.placeholder = 'Type text to search country..';
        this.options.searchIcon = 'assets/icons/add-user.svg';
        this.options.type = 'search';

        //this.profileData();

        this.cameraOption = {
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.CAMERA,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true
        };

        this.galaryOption = {
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true
        };

        storage.get('getProfile').then((val) => {
            this.habitsData = val.habbits;
            this.countriesListArray = val.countries;
            console.log('this.habitsData', this.habitsData);
        });
    }


    /*    profileData() {
            this.userService.getProfileData().then((data) => {
                this.countries = data['countries'];
            });
        }*/


    ngOnInit() {
        this.newMessage = localStorage.getItem('campus-app-chat-message');
        this.events.publish('feedbackPopup');
        this.userService.getProfileData();
        this.userData = this.userService.getLoggedInUserData();
        this.countryFilter
            .debounceTime(500)
            .subscribe((keyword) => {
                this.selectedCountry = keyword.value;
                this.showCuntryList=true;
                this.countriesList = this.countriesListArray.filter((country) => {
                     return country.name.toString().toLowerCase().startsWith(
                        keyword.value.toString().toLowerCase()
                    );
                });
                console.log('this.countriesList', this.countriesList);
            });

        this.stateFilter
            .debounceTime(500)
            .subscribe((keyword) => {
                this.selectedState = keyword.value;
                this.showStateList=true;
                this.stateList = this.stateListArray.filter((state) => {
                    return state.name.toString().toLowerCase().startsWith(
                        keyword.value.toString().toLowerCase()
                    );
                });

                console.log('this.countriesList', this.countriesList);
            });



    }

    ionViewWillEnter() {

        this.getUserData();

        if (!this.userData) {
            // this.getUserData();
        } else {
            this.profileform.firstname = this.userData.first_name;
            this.profileform.lastname = this.userData.last_name;
        }
    }

    getUserData() {
        this.httpService.get('user/profile_show')
            .subscribe(
                (data: any) => {
                    console.log(data);
                    this.userData = data.data;
                    this.profileform.firstname = this.userData.first_name;
                    this.profileform.lastname = this.userData.last_name;
                    if (this.userData['user_profile'] != null) {
                        this.profileform.bio = this.userData['user_profile']['bio'];
                        this.profileform.status = JSON.stringify(this.userData['user_profile']['status']);
                        if (this.userData['user_profile']['sex'] != null) {
                            this.genderIndex = this.userData['user_profile']['sex'];
                        }
                        if (this.userData['user_profile']['country'] != null) {
                            this.countryId = this.userData['user_profile']['country']['id'];
                            this.selectedCountry = this.userData['user_profile']['country']['name'];
                        }
                        if (this.userData['user_profile']['state'] != null) {
                            this.stateId = this.userData['user_profile']['state']['id'];
                            this.selectedState = this.userData['user_profile']['state']['name'];
                        }
                        this.genderId = this.genderImages[this.genderIndex].id;
                    }
                    this.user_interestsArray = [];
                    if (this.userData['user_interests'] != undefined) {
                        this.user_interests = this.userData['user_interests'];
                        if (this.user_interests && this.user_interests.length > 0) {
                            for (let i = 0; i < this.user_interests.length; i++) {
                                this.user_interestsArray.push(this.user_interests[i].interest.id);
                            }
                        }
                    }


                    if (this.userData['user_profile_picture'] != null) {
                        this.profileUrl = this.userData.user_profile_picture.profile_picture;
                    } else {
                        this.setFbProfilePic();
                    }
                    this.socialArray = this.userData['user_social_links'];
                    if (data.data['user_habbits'].length > 0) {
                        this.setHabits(data.data['user_habbits']);
                    }
                    this.changeRef.detectChanges();
                    // this.loader.dismiss();
                },
                (err: any) => {
                    // this.loader.dismiss();
                    throw(err);
                });
    }


    async setHabits(user_habbit) {


        for (let i = 0; i < user_habbit.length; i++) {
            for (let j = 0; j < this.habitsData.length; j++) {
                if (this.habitsData[j].habbit_name === user_habbit[i].habbit.habbit_name) {
                    switch (user_habbit[i].habbit.habbit_name) {
                        case 'diet': {
                            if (user_habbit[i].habbit_option != null) {
                                this.dietIndex = this.habitsData[j].get_habbit_option.findIndex(x => x.habbit_option_name === user_habbit[i].habbit_option.habbit_option_name);
                                this.dietHabitId = this.habitsData[j].get_habbit_option[this.dietIndex].id;
                                console.log(this.dietIndex, this.dietHabitId);
                            }
                            break;
                        }
                        case 'smoking': {
                            if (user_habbit[i].habbit_option != null) {
                                this.smokingIndex = this.habitsData[j].get_habbit_option.findIndex(x => x.habbit_option_name === user_habbit[i].habbit_option.habbit_option_name);
                                this.smokingHabitId = this.habitsData[j].get_habbit_option[this.smokingIndex].id;
                                console.log(this.smokingIndex, this.smokingHabitId);
                            }
                            break;
                        }
                        case 'drinking': {
                            if (user_habbit[i].habbit_option != null) {
                                this.drinkingIndex = this.habitsData[j].get_habbit_option.findIndex(x => x.habbit_option_name === user_habbit[i].habbit_option.habbit_option_name);
                                this.drinkingHabitId = this.habitsData[j].get_habbit_option[this.drinkingIndex].id;
                                console.log(this.drinkingIndex, this.drinkingHabitId);
                            }
                            break;
                        }
                        case 'sleeping': {
                            if (user_habbit[i].habbit_option != null) {
                                this.sleepingIndex = this.habitsData[j].get_habbit_option.findIndex(x => x.habbit_option_name === user_habbit[i].habbit_option.habbit_option_name);
                                this.sleepingHabitId = this.habitsData[j].get_habbit_option[this.sleepingIndex].id;
                                console.log(this.sleepingHabitId);
                            }
                            break;
                        }

                        default: {
                            //statements;
                            break;
                        }


                    }
                }

            }
        }


    }

    setFbProfilePic() {
        this.storage.get('facebookUrl').then(value => {
            if (value != null) {
                console.log('val', value);
                if (this.profileUrl === null) {
                    this.profileUrl = value;
                }
                let index;
                let facebook = this.socialArray.find(x => x.social_provider === 'FACEBOOK');
                if (facebook != undefined) {
                    this.facebook = facebook.social_link;
                    index = this.socialArray.findIndex(x => x.social_provider === 'FACEBOOK');
                    this.socialArray.splice(index, 1);
                }

                let obj = {'social_provider': 'FACEBOOK', 'social_link': value};
                this.socialArray.push(obj);
                console.log('this.socialArray', this.socialArray);
            }
        });
    }


    async editProfile() {

        const alert = await this.alertController.create({
            header: 'Image Selection',
            buttons: [
                {
                    text: 'Camera',
                    handler: () => {
                        this.captureImage();
                    }
                }, {
                    text: 'Gallery',
                    handler: () => {
                        this.imageFromGallary();
                    }
                }
            ]
        });

        await alert.present();


    }

    private captureImage(): void {
        let win: any = window;
        this.camera.getPicture(this.cameraOption).then(
            (imageData) => {
                let base64Image = 'data:image/jpeg;base64,' + imageData;
                // let fileUriData = fileUri.split('?')[0];
                // this.profileUrl = win.Ionic.WebView.convertFileSrc(fileUriData);
                this.profileUrl = base64Image;
                console.log('fileUri', this.profileUrl);
                let body = {
                    profile_picture: this.profileUrl
                };


                console.log('body', body);

                this.userService.updateProfilePicture(body);
            }, (err) => {
                // Handle error
            });
    }

    async changeStatus(event) {
        console.log(event.target.value);
        this.profileform.status = event.target.value;
    }

    private imageFromGallary(): void {
        let win: any = window;

        this.camera.getPicture(this.galaryOption).then(
            (imageData) => {
                let base64Image = 'data:image/jpeg;base64,' + imageData;
                // let fileUriData = fileUri.split('?')[0];
                // this.profileUrl = win.Ionic.WebView.convertFileSrc(fileUriData);
                this.profileUrl = base64Image;
                console.log('fileUri', this.profileUrl);
                let body = {
                    profile_picture: this.profileUrl
                };


                console.log('body', body);

                this.userService.updateProfilePicture(body);

            }, (err) => {
                // Handle error
            });
    }

    selectCountry(res){
        this.selectedCountry = res.name;
        this.showCuntryList=false;
        this.countryId = res.id;
        this.selectedState = null;
        this.stateId = null;

        this.userService.getStateData(this.countryId).then((response) => {
          this.stateListArray=response;
        });
    }

    selectState(res){
        this.selectedState = res.name;
        this.showStateList=false;
        this.stateId = res.id;
    }

    on(output, event): void {
        if (event.id !== undefined) {
            console.log('id', event.id);
            this.countryId = event.id;
            this.countryCode = event.code;
            this.stateComplet.countryID(this.countryId);
            this.storage.set('countryId', event.id);
        }

    }

    onState(output, event): void {
        if (event.id !== undefined) {
            console.log('id', event.id);
            this.stateId = event.id;
            this.stateCode = event.name;
        }
    }

    onPressGender($event) {
        console.log('onPress', $event);
        this.startIntervalGender();
    }

    onPressUpGender($event) {
        console.log('onPressUp', $event);
        this.stopIntervalGender();
    }

    startIntervalGender() {
        this.progressGender = 0;
        const self = this;
        this.intervalGender = setInterval(function () {
            self.progressGender = self.progressGender + 1;
            if (self.progressGender === 20) {
                self.changeGender();
            }
        }, 50);
    }

    stopIntervalGender() {
        clearInterval(this.intervalGender);
    }

    changeGender() {
        let length = this.genderImages.length;
        if (length === this.genderIndex + 1) {
            this.genderIndex = 0;
        } else {
            this.genderIndex++;
        }

        this.genderId = this.genderImages[this.genderIndex].id;
    }


    onPress($event, habbit_name, get_habbit_option, index) {
        console.log('onPress', $event);
        this.startInterval(habbit_name, get_habbit_option, index);
    }

    onPressUp($event) {
        console.log('onPressUp', $event);
        this.stopInterval();
    }

    startInterval(habbit_name, get_habbit_option, index) {
        this.progress = 0;
        const self = this;
        this.interval = setInterval(function () {
            self.progress = self.progress + 1;
            if (self.progress === 20) {
                self.changeHabitsOption(habbit_name, get_habbit_option, index);
            }
        }, 50);
    }

    stopInterval() {
        clearInterval(this.interval);
    }


    changeHabitsOption(habbit_name, get_habbit_option, index) {
        switch (habbit_name) {
            case 'diet': {
                let length = get_habbit_option.length;
                if (length === this.dietIndex + 1) {
                    this.dietIndex = 0;
                } else {
                    this.dietIndex++;
                }
                this.dietHabitId = get_habbit_option[this.dietIndex].id;
                console.log(this.dietHabitId);
                break;
            }
            case 'smoking': {
                let length = get_habbit_option.length;
                if (length === this.smokingIndex + 1) {
                    this.smokingIndex = 0;
                } else {
                    this.smokingIndex++;
                }
                this.smokingHabitId = get_habbit_option[this.smokingIndex].id;
                console.log(this.smokingHabitId);
                break;
            }

            case 'drinking': {
                let length = get_habbit_option.length;
                if (length === this.drinkingIndex + 1) {
                    this.drinkingIndex = 0;
                } else {
                    this.drinkingIndex++;
                }
                this.drinkingHabitId = get_habbit_option[this.drinkingIndex].id;
                console.log(this.drinkingHabitId);
                break;
            }

            case 'sleeping': {
                let length = get_habbit_option.length;
                if (length === this.sleepingIndex + 1) {
                    this.sleepingIndex = 0;
                } else {
                    this.sleepingIndex++;
                }
                this.sleepingHabitId = get_habbit_option[this.sleepingIndex].id;
                console.log(this.sleepingHabitId);
                break;
            }

            default: {
                //statements;
                break;
            }
        }
    }


    redirectToPage(page) {
        this.router.navigateByUrl('/' + page);
    }

    facebookData() {
        this.fb.getLoginStatus().then((res) => {
            if (res.status === 'connected') {
                // Already logged in to FB so pass credentials to provider (in my case firebase)
                this.logoutFB();

            } else {
                this.getFacebookData();
            }
        });
    }

    logoutFB() {
        this.fb.logout()
            .then(res => {
                console.log('fbLogout', res);
                this.getFacebookData();
            })
            .catch(e => this.getFacebookData());
    }

    getFacebookData() {
        this.fb.login(['public_profile', 'user_friends', 'email'])
            .then((res: FacebookLoginResponse) => {
                console.log('Logged into Facebook!', res);
                let facebookId = res.authResponse.userID;
                let profileUrl = 'https://graph.facebook.com/' + facebookId + '/picture?width=1024&height=1024';
                let facebookProfile = 'https://www.facebook.com/profile.php?id='+facebookId;
                let index;
                let facebook = this.socialArray.find(x => x.social_provider === 'FACEBOOK');
                console.log('.social_link', this.facebook);
                if (facebook != undefined) {
                    this.facebook = facebook.social_link;
                    index = this.socialArray.findIndex(x => x.social_provider === 'FACEBOOK');
                    this.socialArray.splice(index, 1);
                }

                let obj = {'social_provider': 'FACEBOOK', 'social_link': facebookProfile};
                this.socialArray.push(obj);

                console.log('this.facebookUrl', profileUrl, this.socialArray);
            })
            .catch(e => console.log('Error logging into Facebook', e));
    }

    async instagramPrompt() {
        let index;
        let instagram = this.socialArray.find(x => x.social_provider === 'INSTAGRAM');
        if (instagram != undefined) {
            this.instagram = instagram.social_link;
            index = this.socialArray.findIndex(x => x.social_provider === 'INSTAGRAM');
        }
        console.log(this.instagram);
        const alert = await this.alertController.create({
            header: 'Please insert instagram profile Url',
            inputs: [
                {
                    name: 'instagram',
                    type: 'text',
                    placeholder: 'url',
                    value: this.instagram
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Ok',
                    handler: (data) => {
                        console.log('Confirm Ok', data);
                        if (this.instagram != null) {
                            this.socialArray.splice(index, 1);
                        }

                        let obj = {'social_provider': 'INSTAGRAM', 'social_link': data['instagram']};
                        this.socialArray.push(obj);
                        console.log('this.socialArrya', this.socialArray);

                    }
                }
            ],
            backdropDismiss: false
        });

        await alert.present();
    }

    async youtubePrompt() {
        let index;
        let youtube = this.socialArray.find(x => x.social_provider === 'YOUTUBE');
        if (youtube != undefined) {
            this.youtube = youtube.social_link;
            index = this.socialArray.findIndex(x => x.social_provider === 'YOUTUBE');
        }
        const alert = await this.alertController.create({
            header: 'Please insert youtube profile Url',
            inputs: [
                {
                    name: 'youtube',
                    type: 'text',
                    placeholder: 'url',
                    value: this.youtube
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');


                    }
                }, {
                    text: 'Ok',
                    handler: (data) => {
                        if (this.youtube != null) {
                            this.socialArray.splice(index, 1);
                        }

                        let obj = {'social_provider': 'YOUTUBE', 'social_link': data['youtube']};
                        this.socialArray.push(obj);

                        console.log('this.socialArray', this.socialArray);
                        console.log('Confirm Ok');
                    }
                }
            ],
            backdropDismiss: false
        });

        await alert.present();
    }

    async saveProfile() {
        // if(this.profileUrl) {
        //   let body = {
        //       profile_picture:this.profileUrl
        //   }


        // console.log('body', body);

        //     this.userService.updateProfilePicture(body);
        // }
        let data = {
            // 'profile_picture': this.profileUrl,
            'bio': this.profileform.bio,
            'first_name': this.profileform.firstname,
            'last_name': this.profileform.lastname,
            'status': this.profileform.status,
            'state': this.stateId,
            'country': this.countryId,
            'sex': this.genderId,
            'user_interests': this.user_interestsArray,
            'user_habbits': [
                {'habbit_id': 1, 'habbit_option': this.dietHabitId},
                {'habbit_id': 2, 'habbit_option': this.smokingHabitId},
                {'habbit_id': 3, 'habbit_option': this.drinkingHabitId},
                {'habbit_id': 4, 'habbit_option': this.sleepingHabitId}
            ],
            'user_social_links': this.socialArray
        };
        console.log('userData', data);
        this.savedUserData = data;
        // this.userService.postProfileData(data);
        this.loader = await this.loading.create({
            message: 'Please wait...'
        });
        await this.loader.present();
        this.httpService.post('user/profile_update', data)
            .subscribe(
                (data: any) => {
                    // TODO check if loadercal storage is available only then set token.Not sure if such a case can occur in mobile.
                    // let test = data;
                    console.log(data);
                    this.loader.dismiss();
                    console.log(this.savedUserData);
                    this.savedUserData.existingInterest = this.userData['user_interests'];
                    this.userService.saveProfileInfo(this.savedUserData);
                    this.router.navigateByUrl('/interest');
                    // this.router.navigateByUrl('/homesearch');

                },
                (err: any) => {
                    this.loader.dismiss();
                    console.log(err);
                    // this.helperService.showToast(errors.message, 2000, 'bottom');
                });


    }

    async saveUserProfile() {
        // if(this.profileUrl) {
        //   let body = {
        //       profile_picture:this.profileUrl
        //   }


        // console.log('body', body);

        //     this.userService.updateProfilePicture(body);
        // }
        let data = {
            // 'profile_picture': this.profileUrl,
            'bio': this.profileform.bio,
            'first_name': this.profileform.firstname,
            'last_name': this.profileform.lastname,
            'status': this.profileform.status,
            'state': this.stateId,
            'country': this.countryId,
            'sex': this.genderId,
            'user_interests': this.user_interestsArray,
            'user_habbits': [
                {'habbit_id': 1, 'habbit_option': this.dietHabitId},
                {'habbit_id': 2, 'habbit_option': this.smokingHabitId},
                {'habbit_id': 3, 'habbit_option': this.drinkingHabitId},
                {'habbit_id': 4, 'habbit_option': this.sleepingHabitId}
            ],
            'user_social_links': this.socialArray
        };
        console.log('userData', data);
        this.savedUserData = data;
        // this.userService.postProfileData(data);
        this.loader = await this.loading.create({
            message: 'Please wait...'
        });
        await this.loader.present();
        this.httpService.post('user/profile_update', data)
            .subscribe(
                (data: any) => {
                    // TODO check if loadercal storage is available only then set token.Not sure if such a case can occur in mobile.
                    // let test = data;
                    console.log(data);
                    this.loader.dismiss();
                    this.userService.saveProfileInfo(this.savedUserData);
                    this.events.publish('user:updateProfile');
                    this.router.navigateByUrl('/homesearch');
                    // this.router.navigateByUrl('/homesearch');

                },
                (err: any) => {
                    this.loader.dismiss();
                    console.log(err);
                    // this.helperService.showToast(errors.message, 2000, 'bottom');
                });


    }

    openChat() {
        this.router.navigateByUrl('/chatlist');
    }

    pageTobeLoaded(navPageName, id) {
        // this.router.navigateByUrl(navPageName +'/'+ id);
        this.router.navigate([navPageName, id]);
    }

    redirectToNearbyAppartents() {
        if (localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
        }
    }

    openUniversityPage() {
        if (localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
        }
    }

    searchuniversity() {
        this.router.navigateByUrl('/homesearch');
    }

}


