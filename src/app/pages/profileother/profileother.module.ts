import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileotherPageRoutingModule } from './profileother-routing.module';

import { ProfileotherPage } from './profileother.page';
import {AutoCompleteModule} from 'ionic4-auto-complete';
import {AutoCompleteService} from '../../services/auto-complete.service';
import {StateAutoCompleteService} from '../../services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
      ProfileotherPageRoutingModule,
      AutoCompleteModule
  ],
  declarations: [ProfileotherPage],
    providers:[      AutoCompleteService,
        StateAutoCompleteService]
})
export class ProfileotherPageModule {}
