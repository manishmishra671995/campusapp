import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ApartmentreviewPageRoutingModule } from './apartmentreview-routing.module';

import { ApartmentreviewPage } from './apartmentreview.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ApartmentreviewPageRoutingModule
  ],
  declarations: [ApartmentreviewPage]
})
export class ApartmentreviewPageModule {}
