import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApartmentreviewPage } from './apartmentreview.page';

const routes: Routes = [
  {
    path: '',
    component: ApartmentreviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApartmentreviewPageRoutingModule {}
