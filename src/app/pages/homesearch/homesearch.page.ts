import {Component, OnInit, ChangeDetectorRef, ViewChild} from '@angular/core';
import {Subject} from 'rxjs/Rx';
import { Messages } from '../../constants/constants';
import {NavController, Events} from '@ionic/angular';
import {HttpService, HelperService} from '../../services/index';
import {Router} from '@angular/router';

@Component({
    selector: 'homesearch',
    templateUrl: './homesearch.page.html',
    styleUrls: ['./homesearch.page.scss'],
})
export class HomesearchPage implements OnInit {
    @ViewChild('mainSearchbar', {static: false}) searchBar;
    searchFieldListener: Subject<any> = new Subject<any>();
    searchFor: string = 'university';
    searchRequestInProgress: boolean = false;
    searchResults: any;
    noResult: boolean = false;;
    apartmentActive: boolean;
    userActive: boolean;
    universityActive: boolean = true;
    newMessage:any;
    followingUniversityId = localStorage.getItem('campus-app-following university-id');
    constructor(
        private httpService: HttpService,
        private navCtrl: NavController,
        private router: Router,
        private events: Events,
        private changeRef: ChangeDetectorRef,
        private helperService: HelperService) {
        console.log('followingUniversityId', this.followingUniversityId)
    }

    ionViewDidEnter() {
        setTimeout(()=>{
            this.searchBar.setFocus();
        }, 150);
    }
    ngOnInit() {
        this.newMessage = localStorage.getItem('campus-app-chat-message');
        this.searchFieldListener
            .debounceTime(500)
            .subscribe((data) => {
                this.getSearchValue(data.index, data.value, data.type);
            });
    }
    onSearchInput(value) {
        if(value == '') {
            this.searchResults = ''
            this.changeRef.detectChanges();
            this.noResult = false;
        }
    }

    searchType(type) {
        this.searchFor = type;
        this.userActive = false;
        this.apartmentActive = false;
        this.universityActive = false;
        if(type == 'user') {
            this.userActive = true;
        }
        if(type == 'apartment') {
            this.apartmentActive = true;
        }
        if(type == 'university') {
            this.universityActive = true;
        }
        this.searchResults = [];
        this.changeRef.detectChanges();
    }

    getSearchValue(index, value, search_option) {
        console.log(index, value, search_option);
        if(!value || value == '')  {
            this.searchResults = ''
            this.changeRef.detectChanges();
            return;
        }
        this.noResult = false;
        let data = {'search_string': value};
        let searchUrl = 'university/search';
        if (this.searchFor == 'university') {
            searchUrl = 'university/search';
        }
        if (this.searchFor == 'user') {
            searchUrl = 'user/search_user';
        }
        if (this.searchFor == 'apartment') {
            searchUrl = 'apartment/search';
        }
        this.searchRequestInProgress = true;
        this.httpService.post(searchUrl, data)

            .subscribe(
                (data: any) => {
                    this.searchRequestInProgress = false;
                    console.log('data', data);
                    this.searchResults = data.data;
                    if(!data.data || data.data.length < 1) {
                        this.noResult = true;
                    } else {
                        this.noResult = false;
                    }
                    console.log(this.searchResults);
                    this.changeRef.detectChanges();

                },
                (err: any) => {
                    this.searchRequestInProgress = false;
                    console.log(err);
                    throw(err);
                    // this.helperService.showToast('Session Expired. Please login to continue', 2000, 'bottom');

                });
    }

    followUniversity(index, id, condition, changeTo) {
        console.log(index, id);
        let data = {'university_id': id};
        let apiUrl = 'university/' + condition;
        this.httpService.post(apiUrl, data)
            .subscribe(
                (data: any) => {
                    console.log('data', data);
                    this.events.publish('followUnfollowUniversity', id);
                    this.searchResults[index].is_following = changeTo;
                },
                (err: any) => {
                    console.log(err);
                    throw(err);

                });
    }

    pageTobeLoaded(navPageName, id) {
        // this.router.navigateByUrl(navPageName +'/'+ id);
        this.router.navigate([navPageName, id]);
    }
    redirectToUserProfile(searchValue, id) {
        console.log(searchValue)
        if(searchValue.is_friend == '1') {
            this.router.navigate(['friend-profile', searchValue.id]);
        } else {
            this.router.navigate(['addnewusercard', searchValue.id]);
        }
        // this.router.navigateByUrl(navPageName +'/'+ id);
    }
    goBack() {
        this.navCtrl.back();
    }

    showRommatesNearUniversity(id) {
      console.log(id)
    }

    redirectToPage(page) {
        this.router.navigateByUrl('/'+page);
    }

    redirectToNearbyAppartents() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
        }
    }
    openUniversityPage() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
        }
    }

    openChat() {
        this.router.navigateByUrl('/chatlist');
    }

}
