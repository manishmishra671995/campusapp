import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomesearchPageRoutingModule } from './homesearch-routing.module';

import { HomesearchPage } from './homesearch.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomesearchPageRoutingModule
  ],
  declarations: [HomesearchPage]
})
export class HomesearchPageModule {}
