import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomesearchPage } from './homesearch.page';

describe('HomesearchPage', () => {
  let component: HomesearchPage;
  let fixture: ComponentFixture<HomesearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomesearchPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomesearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
