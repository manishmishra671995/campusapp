import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {HelperService, HttpService} from '../../services';
import { Router } from '@angular/router';
import { Messages } from '../../constants/constants';
import {LoadingController, Events} from '@ionic/angular';

@Component({
  selector: 'notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {
	allNotifications:any;
  notificationActive: boolean =  true;
  noNotification: boolean =  false;
  newMessage:any;
  private loader:any;
  	constructor(private httpService: HttpService,
                private loading: LoadingController,
  				      private changeRef: ChangeDetectorRef,
  				      private router: Router,
                private events: Events,
			         	private helperService: HelperService) { }

  	ngOnInit() {
      this.newMessage = localStorage.getItem('campus-app-chat-message');
      this.getNotifications();
	  	this.events.publish('feedbackPopup');
  	}

    async getNotifications() {
      this.loader = await this.loading.create({
        message: 'Please wait...'
      });
      await this.loader.present();
      this.httpService.get('notification/get')
        .subscribe(
            (data: any) => {
              console.log(data);
              this.loader.dismiss();
              this.allNotifications = data.data;
              console.log(this.allNotifications);
              if(data.data.length < 1) {
                this.noNotification = true;
              }
              this.changeRef.detectChanges();
                
            },
        (err: any) => {
            this.loader.dismiss();
            throw(err);
        });
    }

  	searchuniversity() {
  		this.router.navigateByUrl('/homesearch');
  	}

     redirectToPage(page) {
      this.router.navigateByUrl('/'+page);
    }

    redirectToNearbyAppartents() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
        }
    }
    openUniversityPage() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
        }
    }

    clickedNotification(notification) {
      notification.is_read = '1';
       this.markNotificationRead(notification.id);
      if(notification.notification_type.toLowerCase() == "apartment_apply") {
        let apartmentId = notification.apartment_id;
        this.router.navigate(['apartmentdetail', apartmentId, notification.application_id, notification.apartment_room_id]);
      }
      if(notification.notification_type.toLowerCase() == "friends_request") {
        this.router.navigate(['roomatesearch/notification', notification.user_id]);
      }
      if(notification.notification_type.toLowerCase() == "chat_message") {
        this.router.navigate(['chat', notification.from_id]);
      }
    }

    openChat() {
      this.router.navigateByUrl('/chatlist');
    }

    markNotificationRead(id) {
      this.httpService.get('notification/read/' + id)
        .subscribe(
          (data: any) => {
            this.changeRef.detectChanges();
              
          },
        (err: any) => {
            throw(err);
        });
    }

    getNotificationType(type) {
      return type.replace('_', ' ');    
    }


}
