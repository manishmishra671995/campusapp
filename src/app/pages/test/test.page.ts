import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {Geolocation} from '@ionic-native/geolocation/ngx';

declare var google;


@Component({
    selector: 'test',
    templateUrl: './test.page.html',
    styleUrls: ['./test.page.scss'],
})
export class TestPage implements OnInit {
    @ViewChild('map', {static: false}) mapElement: ElementRef;
    map: any;

    museumData = [
        {
            'name': 'National Museum',
            'state': 'Delhi',
            'latitude': 28.6117993,
            'longitude': 77.2194934
        },
        {
            'name': 'National Science Centre,',
            'state': 'Delhi',
            'latitude': 28.6132098,
            'longitude': 77.245437
        },
        {
            'name': 'The Sardar Patel Museum',
            'state': 'Gujrat',
            'latitude': 21.1699005,
            'longitude': 72.7955734
        },
        {
            'name': 'Library of Tibetan Works and Archives',
            'state': 'Himachal',
            'latitude': 32.2263696,
            'longitude': 76.325326

        },
        {
            'name': 'Chhatrapati Shivaji Maharaj Vastu Sangrahalaya',
            'state': 'Maharashtra',
            'latitude': 18.926873,
            'longitude': 72.8326132
        },
        {
            'name': 'Namgyal Institute of Tibetology',
            'state': 'Sikkim',
            'latitude': 27.315948,
            'longitude': 88.6047829

        }
    ];

    constructor(private geolocation: Geolocation
    ) {
    }

    ngOnInit() {
        // this.loadMap();
        this.displayGoogleMap();
        this.getMarkers();

    }


    displayGoogleMap() {
        const latLng = new google.maps.LatLng(28.6117993, 77.2194934);

        const mapOptions = {
            center: latLng,
            disableDefaultUI: true,
            zoom: 4,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    }


    getMarkers() {
        // tslint:disable-next-line:variable-name
        for (let _i = 0; _i < this.museumData.length; _i++) {
            if (_i > 0) {
                this.addMarkersToMap(this.museumData[_i]);
            }
        }
    }

    addMarkersToMap(museum) {
        const position = new google.maps.LatLng(museum.latitude, museum.longitude);
        const museumMarker = new google.maps.Marker({position, title: museum.name});
        museumMarker.setMap(this.map);
    }


    loadMap() {
        this.geolocation.getCurrentPosition().then((resp) => {
            let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
            let mapOptions = {
                center: latLng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            //this.getAddressFromCoords(resp.coords.latitude, resp.coords.longitude);

            this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

            this.map.addListener('tilesloaded', () => {
                console.log('accuracy', this.map);
                //this.getAddressFromCoords(this.map.center.lat(), this.map.center.lng())
            });

        }).catch((error) => {
            console.log('Error getting location', error);
        });
    }

}
