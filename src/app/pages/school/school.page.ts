import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {HttpService, HelperService} from '../../services/index';
import {LoadingController, NavController} from '@ionic/angular';
import { Messages } from '../../constants/constants';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'school',
  templateUrl: './school.page.html',
  styleUrls: ['./school.page.scss'],
})
export class SchoolPage implements OnInit {
	private loader: any;
	mutualSchoolsList:any;
	friendId:any;
	mutualSchoolsUnavailable:boolean = false;
  newMessage:any;
  	constructor(private loading: LoadingController,
  				private httpService: HttpService,
  				private route: ActivatedRoute,
          private navCtrl: NavController,
      		  	private changeRef: ChangeDetectorRef,
    		  	private router: Router,
    		  	private helperService: HelperService,) {
  		
	}

  	ngOnInit() {	
    this.newMessage = localStorage.getItem('campus-app-chat-message');
		this.friendId = this.route.snapshot.paramMap.get('id');
  		this.getMutualSchools();
  	}

  	async getMutualSchools() {
  		this.loader = await this.loading.create({
            message: 'Please wait...'
        });

        await this.loader.present();
  		this.httpService.get('university/follow_list')
	    .subscribe(
	        (data: any) => {
	            console.log('friend', data.data);
	            this.mutualSchoolsList = data.data;
	            if(!data.data.length) {
	            	this.mutualSchoolsUnavailable = true;
	            }
	            this.changeRef.detectChanges();
	            this.loader.dismiss();
	                                
	        },
	        (err: any) => {
	            this.loader.dismiss();
	            throw(err);
	        });
  	}

  	searchuniversity() {
	  	this.router.navigateByUrl('/homesearch');
  	}
	openChat() {
      	this.router.navigateByUrl('/chatlist');
    }

    redirectToPage(page) {
        this.router.navigateByUrl('/'+page);
    }

    redirectToNearbyAppartents() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
        }
    }
    openUniversityPage() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
        }
    }

    goToUniversity(id) {
      let navPageName = 'universitydetail';
      this.router.navigate([navPageName, id]);
    }

    goBack() {
      this.navCtrl.back();
    }

}
