import {Component, OnInit} from '@angular/core';
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook/ngx';
import {GooglePlus} from '@ionic-native/google-plus/ngx';
import {Router} from '@angular/router';
import {LoadingController, Platform, MenuController} from '@ionic/angular';
import {UserService} from '../../services';
import {UserData} from '../../providers/user-data';
import {HttpClient} from '@angular/common/http';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'welcome',
    templateUrl: './welcome.page.html',
    styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
    facebookImgUrl: any;

    userData = {};
    deviceToken: any='';

    constructor(private fb: Facebook,
                private googlePlus: GooglePlus,
                private router: Router,
                private menu: MenuController,
                private loading: LoadingController,
                public plt: Platform,
                private userService: UserService,
                private storage: Storage
    ) {
        this.storage.get('deviceToken').then((val) => {
            console.log('deviceToken-->', val);
            if (val != null) {
                this.deviceToken = val;
            }
        });
    }

    ngOnInit() {
        this.menu.enable(false);
    }

    exitApp() {
        navigator['app'].exitApp();
    }

    loginWithFB() {

        this.fb.getLoginStatus().then((res) => {
            if (res.status === 'connected') {
                // Already logged in to FB so pass credentials to provider (in my case firebase)
                this.logoutFB();

            } else {
                this.getFacebookData();
            }
        });
    }

    logoutFB() {
        this.fb.logout()
            .then(res => {
                console.log('fbLogout', res);
                this.getFacebookData();
            })
            .catch(e => this.getFacebookData());
    }

    getFacebookData() {
        this.fb.login(['public_profile', 'user_friends','user_link', 'email'])
            .then((res: FacebookLoginResponse) => {
                console.log('Logged into Facebook!', res);
                let facebookId = res.authResponse.userID;
                this.getFBUserDetail(facebookId);
                this.userData = {
                    grant_type: 'social',
                    social_provider: 'facebook',
                    social_token: res.authResponse.accessToken,
                    device_type: 'android',
                    device_id: this.deviceToken
                };
                this.facebookImgUrl = 'https://graph.facebook.com/' + facebookId + '/picture?width=1024&height=1024';


                this.storage.set("facebookUrl", this.facebookImgUrl);

                this.userService.socialLogin(this.userData);

                console.log('this.facebookUrl', this.facebookImgUrl);
            })
            .catch(e => console.log('Error logging into Facebook', e));
    }

    getFBUserDetail(userid) {
        this.fb.api('/' + userid + '/?fields=id,email,name,picture,gender,link', ['public_profile'])
            .then(res => {
                console.log(JSON.stringify(res));
                this.facebookImgUrl = 'https://graph.facebook.com/' + res.id + '/picture?width=1024&height=1024';
                console.log(this.facebookImgUrl);
                // alert(JSON.stringify(res));

            })
            .catch(e => {
                console.log(e);
            });
    }

    async loginWithGoogle() {
        this.googlePlus.login({
            'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
            'webClientId': '1070885639941-k116lh1b586ishpcmqlig2qeqqaip4et.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
            'offline': true // Optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
        })
            .then(user => {
                console.log('ss' + JSON.stringify(user));
                this.userData = {
                    grant_type: 'social', social_provider: 'google', social_token: user.accessToken,
                    device_type: 'android',
                    device_id: this.deviceToken
                };

                this.userService.socialLogin(this.userData);

            }, err => {
               // alert('error' + JSON.stringify(err));
            });

    }


    /*this.googlePlus.login({})
    .then(res => console.log(res))
    .catch(err => console.error(err));*/


    login() {
        this.router.navigateByUrl('/login');
    }

    createaccount() {

        this.router.navigateByUrl('/createaccount1');
    }
    policyPage() {
        let url = 'https://campus.computer/privacy_policy';

        window.open(url, '_blank');
    }
    termsAndService() {
        let url = 'https://campus.computer/terms_and_conditions';

        window.open(url, '_blank');
    }
}
