import { Component, OnInit, ChangeDetectorRef } from '@angular/core';

import { PopoverController, LoadingController, NavController } from '@ionic/angular';
import { Messages } from '../../constants/constants';
import { PopoverPage } from '../about-popover/about-popover';
import { ActivatedRoute } from '@angular/router';
import {HttpService, HelperService} from '../../services/index';
import { Router } from '@angular/router';

@Component({
  selector: 'universityreview',
  templateUrl: './universityreview.page.html',
  styleUrls: ['./universityreview.page.scss'],
})
export class UniversityreviewPage implements OnInit {
  universityId:any;
  appartmentId:any;
  reviewType:any;
  universityData: any;
  apartmentData: any;
  selfReviews: any = [];
  othersReviews: any = [];
  totalSelfRating:any;
  newRating:any = 0;
  editedReview;
  loggedInUseId = localStorage.getItem('campus-app-logged-in-user-id');
  loggedInUserDetails = JSON.parse(localStorage.getItem('campus-app-logged-in-user'));
  reviewDetailsLoaded:boolean = false; 
  editreview:boolean = false; 
  editing:boolean = false; 
  addRating:boolean = false; 
  private loader: any;
  newMessage:any;
  constructor(public popoverCtrl: PopoverController,
              private route: ActivatedRoute,
              private loading: LoadingController,
              private router: Router,
              private navCtrl: NavController,
              private httpService: HttpService,
              private helperService: HelperService,
              private changeRef: ChangeDetectorRef) {
              }

  ngOnInit() {
    this.newMessage = localStorage.getItem('campus-app-chat-message');
    this.universityId = this.route.snapshot.paramMap.get('id');
    this.reviewType = this.route.snapshot.paramMap.get('type');
    if(this.reviewType == 'university') {
      this.getUnversityDetails(this.universityId); 
    } else {
      this.appartmentId = this.universityId;
      this.getAppartmentDetails(this.appartmentId); 
    }
  } 

  async presentPopover(event: Event) {
    const popover = await this.popoverCtrl.create({
      component: PopoverPage,
      event
    });
    await popover.present();
  }
  async getUnversityDetails(universityId) {
    this.loader = await this.loading.create({
            message: 'Please wait...'
        });

        await this.loader.present();
      let appartmentUrl = 'university/get_reviews/'+universityId;
      this.httpService.get(appartmentUrl)
        .subscribe(
            (data: any) => {
                this.loader.dismiss();
                this.universityData = data.data;
                this.reviewDetailsLoaded = true;
                this.sortReviews();
            },
            (err: any) => {
                this.loader.dismiss();
                throw(err);
            });
  }
  /*get Apartments Detail*/
  async getAppartmentDetails(appartmentId) {
    this.loader = await this.loading.create({
      message: 'Please wait...'
    });

        await this.loader.present();
      let appartmentUrl = 'apartment/get_reviews/'+appartmentId;
      this.httpService.get(appartmentUrl)
        .subscribe(
            (data: any) => {
                this.loader.dismiss();
                this.apartmentData = data.data;
                console.log(this.apartmentData);
                this.reviewDetailsLoaded = true;
                this.sortApartmentReviews();
            },
            (err: any) => {
                this.loader.dismiss();
                throw(err);
            });
  }

  sortApartmentReviews() {
    this.selfReviews = [];
    this.othersReviews = [];
    if(this.apartmentData.ratings.length) {
      for(var i = 0; i < this.apartmentData.ratings.length; i++) {
        if(this.apartmentData.ratings[i].user.id == this.loggedInUseId) {
          this.selfReviews.push(this.apartmentData.ratings[i]);
          this.editedReview = this.apartmentData.ratings[i].review;
          this.totalSelfRating = this.apartmentData.ratings[i].rating;
        } else {
          this.othersReviews.push(this.apartmentData.ratings[i]);
        }
      }
      // this.reviewDetailsLoaded = true;
      console.log(this.selfReviews, this.othersReviews)
      this.changeRef.detectChanges();
    }
  }

  async deleteReview() {
    console.log(this.selfReviews);
    let reviewId = this.selfReviews[0].id;
    this.loader = await this.loading.create({
      message: 'Please wait...'
    });
    await this.loader.present();
    this.httpService.get('university/delete_review/'+reviewId)
      .subscribe(
        (data: any) => {
            this.loader.dismiss();
            this.editing = false;
            this.editedReview = '';
            this.editreview = false;
            this.getUnversityDetails(this.universityId);
        },
        (err: any) => {
            this.loader.dismiss();
            throw(err);
        });
  }
  async deleteApartmentReview() {
    console.log(this.selfReviews);
    let reviewId = this.selfReviews[0].id;
    this.loader = await this.loading.create({
      message: 'Please wait...'
    });
    await this.loader.present();
    this.httpService.get('apartment/delete_review/'+reviewId)
      .subscribe(
        (data: any) => {
            this.loader.dismiss();
            this.editing = false;
            this.editedReview = '';
            this.editreview = false;
            this.getAppartmentDetails(this.universityId);
        },
        (err: any) => {
            this.loader.dismiss();
            throw(err);
        });
  }

  async saveApartmentReview() {
    console.log(this.totalSelfRating)
    this.loader = await this.loading.create({
      message: 'Please wait...'
    });
    await this.loader.present();
    let data = {'apartment_id': this.appartmentId, 'rating': this.totalSelfRating, 'review': this.editedReview};
    this.httpService.post('apartment/add_review', data)
      .subscribe(
        (data: any) => {
            this.loader.dismiss();
            this.editing = false;
            this.editreview = false;
            this.getAppartmentDetails(this.appartmentId);
        },
        (err: any) => {
            this.loader.dismiss();
            throw(err);
        });

  }

  /*get Apartments Detail*/


  sortReviews() {
    this.selfReviews = [];
    this.othersReviews = [];
    if(this.universityData.ratings.length) {
      for(var i = 0; i < this.universityData.ratings.length; i++) {
        if(this.universityData.ratings[i].user.id == this.loggedInUseId) {
          this.selfReviews.push(this.universityData.ratings[i]);
          this.editedReview = this.universityData.ratings[i].review;
          this.totalSelfRating = this.universityData.ratings[i].rating;
        } else {
          this.othersReviews.push(this.universityData.ratings[i]);
        }
      }
      this.reviewDetailsLoaded = true;
      this.changeRef.detectChanges();
      console.log(this.selfReviews, this.othersReviews)
    }
  }

  showHideEditPopup() {
    if(this.editreview === true) {
      this.editreview = false;
    } else {
      this.editreview = true;
    }
    this.changeRef.detectChanges();
  }

  editSelfReview() {
    this.editing = true;
    this.editreview = false;
    this.changeRef.detectChanges();
  }

  addReview() {
    this.editing = true;
    this.editreview = false;
    this.addRating = true;
    this.changeRef.detectChanges();
  }

  saveRating(rating) {
    if(this.editing === true) {
      this.totalSelfRating = rating;
      this.selfReviews[0].rating = this.totalSelfRating;
      this.changeRef.detectChanges();
    }
  }
  saveNewRating(rating) {
    this.newRating = rating;
  }

  async saveReview() {
    this.loader = await this.loading.create({
      message: 'Please wait...'
    });
    await this.loader.present();
    let data = {'university_id': this.universityId, 'rating': this.totalSelfRating, 'review': this.editedReview};
    this.httpService.post('university/add_review', data)
      .subscribe(
        (data: any) => {
            this.loader.dismiss();
            this.editing = false;
            this.editreview = false;
            this.getUnversityDetails(this.universityId);
        },
        (err: any) => {
            this.loader.dismiss();
            throw(err);
        });

  }
  async saveNewReview() {
    console.log(this.universityId, this.newRating, this.editedReview)
    this.loader = await this.loading.create({
      message: 'Please wait...'
    });
    await this.loader.present();
    let data = {'university_id': this.universityId, 'rating': this.newRating, 'review': this.editedReview};
    this.httpService.post('university/add_review', data)
      .subscribe(
        (data: any) => {
            this.loader.dismiss();
            this.editing = false;
            this.addRating = false;
            this.editreview = false;
            this.getUnversityDetails(this.universityId);
        },
        (err: any) => {
            this.loader.dismiss();
            throw(err);
        });

  }
  async saveNewAppartmentReview() {
    console.log(this.universityId, this.newRating, this.editedReview)
    this.loader = await this.loading.create({
      message: 'Please wait...'
    });
    await this.loader.present();
    let data = {'apartment_id': this.appartmentId, 'rating': this.newRating, 'review': this.editedReview};
    this.httpService.post('apartment/add_review', data)
      .subscribe(
        (data: any) => {
            this.loader.dismiss();
            this.editing = false;
            this.editreview = false;
            this.addRating = false;
            this.getAppartmentDetails(this.appartmentId); 
        },
        (err: any) => {
            this.loader.dismiss();
            throw(err);
        });

  }
  openChat() {
    this.router.navigateByUrl('/chatlist');
  }

  searchuniversity() {
    this.router.navigateByUrl('/homesearch');
  }
  redirectToNearbyAppartents() {
      if(localStorage.getItem('campus-app-following-university-id')) {
          this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
      } else {
          this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
      }
  }
  openUniversityPage() {
      if(localStorage.getItem('campus-app-following-university-id')) {
          this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
      } else {
          this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
      }
  }
  redirectToPage(page) {
    this.router.navigateByUrl('/'+page);
  }

  pageTobeLoaded(navPageName, id) {
    this.router.navigate([navPageName, id]);
  }
  goBack() {
    this.navCtrl.back();
  }


}
