import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UniversityreviewPage } from './universityreview.page';

const routes: Routes = [
  {
    path: '',
    component: UniversityreviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UniversityreviewPageRoutingModule {}
