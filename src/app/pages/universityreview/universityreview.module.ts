import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UniversityreviewPageRoutingModule } from './universityreview-routing.module';

import { UniversityreviewPage } from './universityreview.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UniversityreviewPageRoutingModule
  ],
  declarations: [UniversityreviewPage]
})
export class UniversityreviewPageModule {}
