import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UniversityreviewPage } from './universityreview.page';

describe('UniversityreviewPage', () => {
  let component: UniversityreviewPage;
  let fixture: ComponentFixture<UniversityreviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniversityreviewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UniversityreviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
