import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import {HttpService, UserService} from '../../services/index';
import {ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import {LoadingController, NavController} from '@ionic/angular';
@Component({
  selector: 'chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  private loader: any;
  userMessage:any;
  allMessages:any;
  userOnChat:any;
  chatWithId:any;
  noMessagesYet:boolean = false;
  userId = localStorage.getItem('campus-app-logged-in-user-id');
  newMessage:any;
  constructor(private socket: Socket,
              private loading: LoadingController,
              private changeRef: ChangeDetectorRef,
              private navCtrl: NavController,
              private router: Router,
              private userService: UserService,
              private route: ActivatedRoute,
              private httpService: HttpService) { 
    this.chatWithId = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.userOnChat = this.userService.getChatUser();
    console.log(this.userOnChat, 'userOnChat');
  	this.socket.connect();
  	this.socket.fromEvent('room_'+this.userId).subscribe((recvdMessage: any)=> {
      if(recvdMessage && recvdMessage.message && recvdMessage.message.to_id == this.userId) {
        this.allMessages.push(recvdMessage.message);
        this.changeRef.detectChanges();
        this.scrollToLastMessage();
      }
    });
    this.getUsersChat();
    this.newMessage = localStorage.getItem('campus-app-chat-message');
  }
  async getUsersChat() {
    this.loader = await this.loading.create({
      message: 'Please wait...'
    });
    await this.loader.present();
    this.httpService.get('chat/message/'+ this.chatWithId)
      .subscribe(
        (data: any) => {
            this.loader.dismiss();
            
            if(!data.data || data.data == null) {
              this.allMessages = [];
            } else {
              this.allMessages = data.data;
            }
            if(!this.allMessages) {
              this.noMessagesYet = true;
            }
            this.changeRef.detectChanges();
            let messagesContainer = document.getElementsByClassName('message');
            if(messagesContainer.length) {
              let scrollTo = document.getElementsByClassName('message')[messagesContainer.length - 1];
             
              var chatht = document.getElementById('chat-box'); 
              var msght = document.getElementById('message-area');
          
              if (chatht.offsetHeight <= msght.offsetHeight) {
                msght.style.cssText = "height:75vh;top:20px;";
              }
              else {
                msght.style.height = "auto";
              }

              scrollTo.scrollIntoView({ behavior: "smooth" })
            }
        },
        (err: any) => {
            this.loader.dismiss();
            throw(err);
            // this.helperService.showToast(errors.message, 2000, 'bottom');
        });
  }

  sendMessage(chatElement) {
    var chatht = document.getElementById('chat-box'); 
    var msght = document.getElementById('message-area');

    if (chatht.offsetHeight <= msght.offsetHeight) {
      msght.style.cssText = "height:75vh;top:20px;";
    }
    else {
      msght.style.height = "auto";
    }

    if(!this.userMessage) {
      chatElement.setFocus();
      return;
    }
    let latestChatdata = {'to_id': 37, 'message': this.userMessage, 'from_id': this.userId};
    this.allMessages.push(latestChatdata);
    this.changeRef.detectChanges();
    let sendObj = {'to_id': this.chatWithId, 'message': this.userMessage};
    let messagesContainer = document.getElementsByClassName('message');
    this.userMessage = '';
    this.scrollToLastMessage();
    chatElement.setFocus();
    this.httpService.post('chat/send_message', sendObj)
      .subscribe(
        (data: any) => {
            // TODO check if loadercal storage is available only then set token.Not sure if such a case can occur in mobile.
            // let test = data;
            console.log(data);
            this.userMessage = '';
            // this.scrollToLastMessage();
            // this.loader.dismiss();
            // this.router.navigateByUrl('/homesearch');

        },
        (err: any) => {
            this.loader.dismiss();
            throw(err);
            // this.helperService.showToast(errors.message, 2000, 'bottom');
        });
    if(messagesContainer.length) {
      this.scrollToLastMessage();
    }
    setTimeout (() => {
         this.scrollToLastMessage();
    }, 200);
  }
  scrollToLastMessage() {
    let messagesContainer = document.getElementsByClassName('message');
    if(messagesContainer.length) {
      let scrollTo = document.getElementsByClassName('message')[messagesContainer.length - 1];
      scrollTo.scrollIntoView({ behavior: "smooth" })
    }
  }
  goBack() {
    this.navCtrl.back();
  }

  openChat() {
    this.router.navigateByUrl('/chatlist');
  }

  searchuniversity() {
    this.router.navigateByUrl('/homesearch');
  }

}
