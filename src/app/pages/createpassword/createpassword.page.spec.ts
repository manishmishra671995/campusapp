import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreatepasswordPage } from './createpassword.page';

describe('CreatepasswordPage', () => {
  let component: CreatepasswordPage;
  let fixture: ComponentFixture<CreatepasswordPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatepasswordPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreatepasswordPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
