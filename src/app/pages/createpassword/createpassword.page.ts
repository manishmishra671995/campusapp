import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, LoadingController } from '@ionic/angular';
import { CustomValidators } from '../../validators/custom-validators';
import { HttpService, HelperService, UserService } from '../../services/index';
import { Router } from '@angular/router';
@Component({
  selector: 'createpassword',
  templateUrl: './createpassword.page.html',
  styleUrls: ['./createpassword.page.scss'],
})
export class CreatepasswordPage implements OnInit {
	choosePasswordForm: FormGroup;
  	password_type: string = 'password';
  	userData;
    forgotPasswordObj:any;
  	verfigypassword_type: string = 'password';
  	userDetails = {password:'', confirm_password:''};
  	private loader: any;
  	constructor(
			private httpService: HttpService,
			private navCtrl: NavController,
			private userService: UserService,
			public formBuilder: FormBuilder,
      private router: Router,
            private helperService: HelperService,
            private loading: LoadingController) { 
  	}

  	ngOnInit() {
      this.userData = this.userService.getNewUserObj();
  		this.forgotPasswordObj = this.userService.getForgotPasswordData();
  		console.log(this.userData, 'passUserdata', this.forgotPasswordObj );
	  	this.choosePasswordForm = this.formBuilder.group({
	      password: [this.userDetails.password, Validators.compose([Validators.required, CustomValidators.passwordPattern])],
	      confirm_password: [this.userDetails.confirm_password, Validators.compose([Validators.required])]
	    }, { validator: CustomValidators.matchingPasswords('password', 'confirm_password') });
  	}
  	togglePasswordMode() {   
	    this.password_type = this.password_type === 'text' ? 'password' : 'text';
	}
	togglePasswordMode1() {   
	    this.verfigypassword_type = this.verfigypassword_type === 'text' ? 'password' : 'text';
  	}

	goBack() {
	    //this.router.navigateByUrl('/welcome');
      
      if(this.forgotPasswordObj) {
        this.router.navigateByUrl('/forgetpassword');
      } else {
        this.navCtrl.back();
      }
  	}

  	async registerUser() {
  		console.log(this.userData);
      if(this.forgotPasswordObj) {
        this.loader = await this.loading.create({
            message: 'Please wait...'
        });

        await this.loader.present();
        let data = {'email': this.forgotPasswordObj.email, 'otp': this.forgotPasswordObj.otp, 'password': this.userDetails.password};     

          this.httpService.post('auth/forgot_password_store', data)
            .subscribe(
                (data: any) => {
                    console.log('data', data);
                    this.userService.saveNewUserObj(data.data);
                    this.navCtrl.navigateRoot('/login');
                    this.loader.dismiss();
                },
                (err: any) => {
                  this.loader.dismiss();
                    throw(err);
            });

      } else {
        this.loader = await this.loading.create({
            message: 'Please wait...'
        });

        await this.loader.present();
        let data = {'email': this.userData.email, 'first_name': this.userData.fname, 'last_name': this.userData.lname, 'password': this.userDetails.password};     

          this.httpService.post('auth/register', data)
            .subscribe(
                (data: any) => {
                    console.log('data', data);
                    this.userService.saveNewUserObj(data.data);
                    this.navCtrl.navigateRoot('/otpverify');
                    this.loader.dismiss();
                },
                (err: any) => {
                  this.loader.dismiss();
                    throw(err);
            });
      }
	    
	      
	    // });
  	}

}
