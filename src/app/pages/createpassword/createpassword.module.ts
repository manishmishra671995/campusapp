import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreatepasswordPageRoutingModule } from './createpassword-routing.module';

import { CreatepasswordPage } from './createpassword.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    CreatepasswordPageRoutingModule
  ],
  declarations: [CreatepasswordPage]
})
export class CreatepasswordPageModule {}
