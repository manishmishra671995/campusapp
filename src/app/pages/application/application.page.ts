import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {HelperService, HttpService} from '../../services';
import { ActivatedRoute } from '@angular/router';
import { Messages } from '../../constants/constants';
import {Events, LoadingController, AlertController, NavController} from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'application',
  templateUrl: './application.page.html',
  styleUrls: ['./application.page.scss'],
})
export class ApplicationPage implements OnInit {
	appartmentId: any;
  roomId: any;
  selectedFriends = [];
	usersFriends: any;
	appartmentDetail: any;
	loader: any;
	allNotifications:any;
  newMessage:any;
  monthlyExpenses:any = [];
  oneTimeExpenses:any = [];
 	constructor(private httpService: HttpService,
 				private route: ActivatedRoute,
 				private loading: LoadingController,
 				private changeRef: ChangeDetectorRef,
 				private router: Router,
        private navCtrl: NavController,
        public alertCtrl: AlertController,
				private helperService: HelperService) { }

	ngOnInit() {
    this.newMessage = localStorage.getItem('campus-app-chat-message');
		this.appartmentId = this.route.snapshot.paramMap.get('id');
		this.roomId = this.route.snapshot.paramMap.get('roomid');
	  	this.getAppartmentDetails(this.appartmentId);
      this.httpService.get('user/get_friends_list')
        .subscribe(
            (data: any) => {
                // this.loader.dismiss();
                console.log(data.data);
                this.usersFriends = data.data;
                
            },
            (err: any) => {
                // this.loader.dismiss();
                throw(err);
            });
  	}
  	async getAppartmentDetails(appartmentId) {
  		this.loader = await this.loading.create({
            message: 'Please wait...'
        });

      await this.loader.present();
  		let appartmentUrl = 'apartment/show/'+appartmentId;
  		this.httpService.get(appartmentUrl)
            .subscribe(
                (data: any) => {
                    this.loader.dismiss();
                    console.log(data.data);
                    this.appartmentDetail = data.data;
                    if(this.appartmentDetail && this.appartmentDetail.rooms) {
                      for(let i = 0; i < this.appartmentDetail.rooms.length; i++) {
                        if(this.appartmentDetail.rooms[i].id == this.roomId) {
                          for (let j = 0; j < this.appartmentDetail.rooms[i].room_expenses.length; j++) {
                            if(this.appartmentDetail.rooms[i].room_expenses[j].expense_type == '1') {
                              this.monthlyExpenses.push(this.appartmentDetail.rooms[i].room_expenses[j])
                            }
                            if(this.appartmentDetail.rooms[i].room_expenses[j].expense_type == '2') {
                              this.oneTimeExpenses.push(this.appartmentDetail.rooms[i].room_expenses[j]);
                            }
                          }
                        }
                      }
                      console.log(this.monthlyExpenses, this.oneTimeExpenses)
                    }
                    this.changeRef.detectChanges();
                    
                },
                (err: any) => {
                    this.loader.dismiss();
                    throw(err);
                });
  	}

  	async applyForAppartment(appartment_id, appartment_room_id, max_applicant) {
      if(this.selectedFriends.length > max_applicant - 1) {
        let alertMessage = 'Only '+ max_applicant + ' applicant are allowed.'
        const alert = await this.alertCtrl.create({
            header: 'Applicants Exceeded.',
            message: alertMessage,
            buttons: [ {
                text: 'Close',
                handler: () => {
                    //this.nav.push(MyNotificationPage);

                    //TODO: Your logic here
                    // this.nav.push(DetailsPage, { message: data.message });
                }
            }]
        });
        await alert.present();
        return;
      }
      if( this.selectedFriends.length > (max_applicant - 1)) {
        let max_applicant_message = 'Max applicant allowed ' + max_applicant ;
        const alert = await this.alertCtrl.create({
          header: 'Max Occupants',
          message: max_applicant_message,
          buttons: ['Dismiss']
        });
        await alert.present();
        return;
      }
  		this.loader = await this.loading.create({
            message: 'Please wait...'
        });

        await this.loader.present();
        let apartmentApplyData = {
			"apartment_id": appartment_id,
			"apartment_room_id":appartment_room_id,
			"co_applicants": this.selectedFriends
		}
  		let appartmentUrl = 'apartment/apply_apartment';
  		this.httpService.post(appartmentUrl, apartmentApplyData)
            .subscribe(
                (data: any) => {
                    this.loader.dismiss();
                    console.log(data.data);
                    this.showSuccessAlert(data.message);
                    
                },
                (err: any) => {
                    this.loader.dismiss();
                    throw(err);
                });
  	}

  	searchuniversity() {
      this.router.navigateByUrl('/homesearch');
    }

    async showSuccessAlert(message) {
      const alert = await this.alertCtrl.create({
          header: 'Success!!',
          message: message,
          buttons: ['Dismiss']
      });
      await alert.present();
    }

    async addFriendforApply() {
      // if(!this.usersFriends) {
      //   return;
      // }
      let friends = [];
      if(this.usersFriends.length < 1) {
        const alert = await this.alertCtrl.create({
          header: 'Add Friend',
          message: 'No Friends Yet.',
          buttons: ['OK']
        });

        await alert.present();
        return;
      }
      console.log('friends', this.usersFriends);
      for (let i = 0; i < this.usersFriends.length; i++) {
        let checked = false;
        if(this.usersFriends[i].checkedValue) {
          checked  = true;
        }
        let user = {'label': this.usersFriends[i].user_friends.first_name + ' '+ this.usersFriends[i].user_friends.last_name, 'type': 'checkbox', 'value': this.usersFriends[i].friend_id, 'checked' : checked};
          friends.push(user);
      }
      console.log(friends)
      const alert = await this.alertCtrl.create({
        header: 'Add Friend',
        inputs: friends,
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: data => {
            }
          },
          {
            text: 'Add',
            handler: data => {
              if(data && data.length) {
                  this.selectedFriends = [];
                  if(this.usersFriends && this.usersFriends.length) {
                    for (let k = 0; k < this.usersFriends.length; k++) {
                      this.usersFriends[k].checkedValue = false;
                    }
                  }
                  
                  for (let k = 0; k < data.length; k++) {

                    for (let f = 0; f < this.usersFriends.length; f++) {
                      if(this.usersFriends[f].friend_id == data[k]){
                        this.usersFriends[f].checkedValue = true;
                        this.selectedFriends.push(this.usersFriends[f].friend_id);
                      }
                      
                    }

                    
                  }
                  console.log('selected Friends', this.usersFriends)
              } else {
                this.selectedFriends = [];
                if(this.usersFriends && this.usersFriends.length) {
                    for (let k = 0; k < this.usersFriends.length; k++) {
                      this.usersFriends[k].checkedValue = false;
                    }
                }
              }
            }
          }
        ]
      });
    await alert.present();
  }

    redirectToPage(page) {
        this.router.navigateByUrl('/'+page);
    }

    showGuide(url) {
      window.open(url, '_blank');
    }

    redirectToNearbyAppartents() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
        }
    }
    goBack() {
      this.navCtrl.back();
    }
    openUniversityPage() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
        }
    }

    openChat() {
      this.router.navigateByUrl('/chatlist');
    }
    getTotalMonthlyExpense(expenses, type) {
      let total = 0;
      for(let i = 0; i < expenses.length; i++) {
        if(expenses[i].expense_type == type) {
          total = parseInt(expenses[i].expense_amount) + total;
        }
        
      }
      return total;
    }
    getTotalMonthlyExpensePerPerson(expenses, type, max_occupants) {
      let total = 0;
      for(let i = 0; i < expenses.length; i++) {
        if(expenses[i].expense_type == type) {
          total = parseInt(expenses[i].expense_amount) + total;
        }
        
      }
      return total / parseInt(max_occupants);
    }

}
