import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {HttpService, HelperService, UserService} from '../../services/index';
import { Messages } from '../../constants/constants';
import {LoadingController, Events, NavController} from '@ionic/angular';
import {Router} from '@angular/router';
@Component({
  selector: 'chatlist',
  templateUrl: './chatlist.page.html',
  styleUrls: ['./chatlist.page.scss'],
})
export class ChatlistPage implements OnInit {

  private loader: any;
  private chatList:any;
  constructor(private httpService: HttpService,
              private helperService: HelperService,
			        private loading: LoadingController,
              private router: Router,
              private navCtrl: NavController,
              private userService: UserService,
              private events: Events,
              private changeRef: ChangeDetectorRef) { }
  ngOnInit() {
    this.removeIfUnreadChat();
  	this.getUsersList();
    this.events.publish('feedbackPopup');
    localStorage.removeItem('campus-app-chat-message');
  }

  removeIfUnreadChat() {
    // let allChatClasses = <HTMLElement>(document.getElementsByClassName('unread-msgs'))
    // if(allChatClasses && allChatClasses.length) {
    //   for (let i = 0; i < allChatClasses.length; i++) {
    //     <HTMLElement>(document.getElementsByClassName('chat-btn'))[i].removeChild(allChatClasses[i])
    //   }
    // }
    const removeElements = (elms) => elms.forEach(el => el.remove());
    removeElements( document.querySelectorAll(".unread-msgs") );
  }

  async getUsersList() {
  	this.loader = await this.loading.create({
      message: 'Please wait...'
    });
    await this.loader.present();
    this.httpService.get('chat/get_users')
      .subscribe(
        (data: any) => {
            this.loader.dismiss();
            console.log(data.data);
            this.chatList = data.data;
            this.changeRef.detectChanges();
        },
        (err: any) => {
            this.loader.dismiss();
            throw(err);
        });
  }

  gotToChat(id, user) {
    this.userService.setChatUser(user);
    this.router.navigate(['chat', id]);
    console.log(user);
  }

  searchuniversity() {
    this.router.navigateByUrl('/homesearch');
  }

  redirectToPage(page) {
        this.router.navigateByUrl('/'+page);
    }

    redirectToNearbyAppartents() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
        }
    }
    openUniversityPage() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
        }
    }
    goBack() {
      this.navCtrl.back();
    }

}
