import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoomatesearchPageRoutingModule } from './roomatesearch-routing.module';

import { RoomatesearchPage } from './roomatesearch.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoomatesearchPageRoutingModule
  ],
  declarations: [RoomatesearchPage]
})
export class RoomatesearchPageModule {}
