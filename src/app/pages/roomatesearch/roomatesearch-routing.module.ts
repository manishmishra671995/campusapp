import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoomatesearchPage } from './roomatesearch.page';

const routes: Routes = [
  {
    path: '',
    component: RoomatesearchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoomatesearchPageRoutingModule {}
