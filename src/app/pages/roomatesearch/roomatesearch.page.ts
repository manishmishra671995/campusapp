import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {HttpService, HelperService} from '../../services/index';
import { Messages } from '../../constants/constants';
import {Events, LoadingController, NavController} from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'roomatesearch',
  templateUrl: './roomatesearch.page.html',
  styleUrls: ['./roomatesearch.page.scss'],
})
export class RoomatesearchPage implements OnInit {
	universityId: any;
  habbitsMaster: any;
  roomtype: any;
	userId: any;
	private loader: any;
  roommatesResults;
	friendRequestResult:any;
  newMessage:any;
  	constructor(private route: ActivatedRoute,
  				private httpService: HttpService,
    			private loading: LoadingController,
          private navCtrl: NavController,
          private changeRef: ChangeDetectorRef,
    			private router: Router,
				private helperService: HelperService,) { }

  	ngOnInit() {
      this.newMessage = localStorage.getItem('campus-app-chat-message');
      this.friendRequestResult = [];
      this.universityId = this.route.snapshot.paramMap.get('id');
  		this.roomtype = this.route.snapshot.paramMap.get('type');
      if(this.roomtype == 'roommate') {
        this.getRoommatesNearUniversity(this.universityId);
      }
      if(this.roomtype == 'notification') {
        this.userId = this.universityId;
  		  this.getFriendRequest(this.userId);
      }
  		this.habbitsMaster = Messages.habitsMaster;
  	}

  	async getRoommatesNearUniversity(universityId) {
      this.loader = await this.loading.create({
            message: 'Please wait...'
        });

        await this.loader.present();
      let appartmentUrl = 'university/get_roommates_by_university/'+universityId;
      this.httpService.get(appartmentUrl)
        .subscribe(
            (data: any) => {
                console.log(data.data);
                this.loader.dismiss();
                this.roommatesFound(data.data);
                                    
            },
            (err: any) => {
                this.loader.dismiss();
                throw(err);
            });
    }

    roommatesFound (roommates) {
      this.roommatesResults = roommates;  
      this.changeRef.detectChanges();
    }
    async getFriendRequest(userId) {
  		this.loader = await this.loading.create({
            message: 'Please wait...'
        });

        await this.loader.present();
  		let appartmentUrl = 'user/get_friends_request';
  		this.httpService.get(appartmentUrl)
            .subscribe(
                (data: any) => {
                    this.loader.dismiss();
                    this.friendRequestResult = data.data;
                    console.log(data.data);
                    console.log(data.data.length);
                    this.loader.dismiss();
                    
                    this.changeRef.detectChanges();
                },
                (err: any) => {
                    this.loader.dismiss();
                    throw(err);
                });
  	}

  	getHabbitImage(habbitId) {
  		for (var i = 0; i < this.habbitsMaster.length; i++) {
  			if(this.habbitsMaster[i].id == habbitId) {
  				return this.habbitsMaster[i].url;
  				break;
  			}
  		}
  	}

  	scrollToNext(number) {
  		var scrollToEventNumber = number + 1;
  		var eventName = 'roommate' + scrollToEventNumber;
  		let b = document.getElementById(eventName);
		 if (b) b.scrollIntoView({ behavior: "smooth" })
  	}

    scrollToPrev(number) {
    	var scrollToEventNumber = number - 1;
    	var eventName = 'roommate' + scrollToEventNumber;
  		let b = document.getElementById(eventName);
		 if (b) b.scrollIntoView({ behavior: "smooth" })
  	}

  	async ignoreUser(userid) {
      this.loader = await this.loading.create({
            message: 'Please wait...'
        });

      await this.loader.present();
      let url = 'user/ignore_user/' + userid;
      this.httpService.get(url)
          .subscribe(
              (data: any) => {
                  this.loader.dismiss();
                  this.helperService.showToast(data.message, 2000, 'bottom');
              },
              (err: any) => {
                  this.loader.dismiss();
                  throw(err);
              });
    }
    async acceptFriendRequest(userid, index) {
      this.loader = await this.loading.create({
            message: 'Please wait...'
        });

      await this.loader.present();
      let data = {'user_friends_id': userid, 'friend_request_accepted': 1};
      this.httpService.post('user/action_friends_request', data)
          .subscribe(
              (data: any) => {
                  this.loader.dismiss();
                  console.log(data.data);
                  this.friendRequestResult[index].is_friend = '1';
                  this.changeRef.detectChanges();
                  this.helperService.showToast(data.message, 2000, 'bottom');
              },
              (err: any) => {
                  this.loader.dismiss();
                  throw(err);
              });
    }
    async rejectFriendRequest(userid, index) {
      this.loader = await this.loading.create({
            message: 'Please wait...'
        });

      await this.loader.present();
      let data = {'user_friends_id': userid, 'friend_request_accepted': 0};
  		this.httpService.post('user/action_friends_request', data)
	        .subscribe(
	            (data: any) => {
                  this.loader.dismiss();
                  console.log(data.data);
                  this.friendRequestResult[index].is_friend = '2';
                  this.changeRef.detectChanges();
	                this.helperService.showToast(data.message, 2000, 'bottom');
	            },
	            (err: any) => {
	                this.loader.dismiss();
	                throw(err);
	            });
  	}

  	async sendFriendRequest(userid, index) {
      this.loader = await this.loading.create({
            message: 'Please wait...'
        });

      await this.loader.present();
      let url = 'user/send_friends_request';
      this.httpService.post(url, {'friend_id' : userid})
          .subscribe(
              (data: any) => {
                  this.loader.dismiss();
                  if(this.roomtype == 'notification') {
                    this.friendRequestResult[index].is_friend = '2';
                  } else {
                    this.roommatesResults[index].is_friend = '2';
                  }
                  this.changeRef.detectChanges();
                  this.helperService.showToast(data.message, 2000, 'bottom');
              },
              (err: any) => {
                  this.loader.dismiss();
                  throw(err);
              });
    }
    openUserProfile(userid) {
  		this.router.navigate(['friend-profile', userid]);
  	}

  	searchuniversity() {
	  	this.router.navigateByUrl('/homesearch');
  	}

    redirectToPage(page) {
        this.router.navigateByUrl('/'+page);
    }

    redirectToNearbyAppartents() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
        }
    }
    openUniversityPage() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
        }
    }
    openChat() {
      this.router.navigateByUrl('/chatlist');
    }

    ngAfterViewInit() {
      if(this.roomtype == 'notification') {
        let usersElementId = 'userId'+this.userId; 
        let usersElement = document.getElementById(usersElementId);
        if (usersElement) usersElement.scrollIntoView({ behavior: "smooth" });
      }
    }
    goBack() {
      this.navCtrl.back();
    }

}
