import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RoomatesearchPage } from './roomatesearch.page';

describe('RoomatesearchPage', () => {
  let component: RoomatesearchPage;
  let fixture: ComponentFixture<RoomatesearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomatesearchPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RoomatesearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
