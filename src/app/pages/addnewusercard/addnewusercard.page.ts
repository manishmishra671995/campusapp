import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {HttpService, HelperService, UserService} from '../../services/index';
import { Messages } from '../../constants/constants';
import {Events, LoadingController, NavController} from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'addnewusercard',
  templateUrl: './addnewusercard.page.html',
  styleUrls: ['./addnewusercard.page.scss'],
})
export class AddnewusercardPage implements OnInit {
	userData:any;
	userId:any;
	habbitsMaster: any;
	private loader: any;
  	constructor(private route: ActivatedRoute,
  				private userService: UserService,
  				private httpService: HttpService,
    			private loading: LoadingController,
          		private navCtrl: NavController,
          		private changeRef: ChangeDetectorRef,
    			private router: Router,
				private helperService: HelperService,) { }

  	ngOnInit() {
  		this.habbitsMaster = Messages.habitsMaster;	
  		this.userId = this.route.snapshot.paramMap.get('id');
  		this.loadUserInfo();
  	}
  	async loadUserInfo() {
  		this.loader = await this.loading.create({
            message: 'Please wait...'
        });
  		this.userService.getOther_profile_show(this.userId).then((data) => {
            console.log('data===>', data);
            this.userData = data;
            this.changeRef.detectChanges();

        });
  	}

  	getHabbitImage(habbitId) {
  		for (var i = 0; i < this.habbitsMaster.length; i++) {
  			if(this.habbitsMaster[i].id == habbitId) {
  				return this.habbitsMaster[i].url;
  				break;
  			}
  		}
  	}

  	async ignoreUser(userid) {
      this.loader = await this.loading.create({
            message: 'Please wait...'
        });

      await this.loader.present();
      let url = 'user/ignore_user/' + userid;
      this.httpService.get(url)
          .subscribe(
              (data: any) => {
                  this.loader.dismiss();
                  this.helperService.showToast(data.message, 2000, 'bottom');
              },
              (err: any) => {
                  this.loader.dismiss();
                  throw(err);
              });
    }

    async acceptFriendRequest(userid, index) {
      this.loader = await this.loading.create({
            message: 'Please wait...'
        });

      await this.loader.present();
      let data = {'user_friends_id': userid, 'friend_request_accepted': 1};
      this.httpService.post('user/action_friends_request', data)
          .subscribe(
              (data: any) => {
                  this.loader.dismiss();
                  console.log(data.data);
                  this.userData	.is_friend = '1';
                  this.changeRef.detectChanges();
                  this.helperService.showToast(data.message, 2000, 'bottom');
              },
              (err: any) => {
                  this.loader.dismiss();
                  throw(err);
              });
    }

    async sendFriendRequest(userid, index) {
      this.loader = await this.loading.create({
            message: 'Please wait...'
        });

      await this.loader.present();
      let url = 'user/send_friends_request';
      this.httpService.post(url, {'friend_id' : userid})
          .subscribe(
              (data: any) => {
                  this.loader.dismiss();
                  this.userData.is_friend = '2'
                  this.changeRef.detectChanges();
                  this.helperService.showToast(data.message, 2000, 'bottom');
              },
              (err: any) => {
                  this.loader.dismiss();
                  throw(err);
              });
    }

    searchuniversity() {
	  	this.router.navigateByUrl('/homesearch');
  	}

  	goBack() {
      this.navCtrl.back();
    }

    redirectToPage(page) {
        this.router.navigateByUrl('/'+page);
    }

    redirectToNearbyAppartents() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
        }
    }
    openUniversityPage() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
        }
    }
    openChat() {
      this.router.navigateByUrl('/chatlist');
    }


}
