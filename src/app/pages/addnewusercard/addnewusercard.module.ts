import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddnewusercardPageRoutingModule } from './addnewusercard-routing.module';

import { AddnewusercardPage } from './addnewusercard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddnewusercardPageRoutingModule
  ],
  declarations: [AddnewusercardPage]
})
export class AddnewusercardPageModule {}
