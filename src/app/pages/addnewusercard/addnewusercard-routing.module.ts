import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddnewusercardPage } from './addnewusercard.page';

const routes: Routes = [
  {
    path: '',
    component: AddnewusercardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddnewusercardPageRoutingModule {}
