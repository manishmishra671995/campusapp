import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddnewusercardPage } from './addnewusercard.page';

describe('AddnewusercardPage', () => {
  let component: AddnewusercardPage;
  let fixture: ComponentFixture<AddnewusercardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddnewusercardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddnewusercardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
