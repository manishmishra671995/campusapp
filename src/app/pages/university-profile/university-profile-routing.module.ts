import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UniversityProfilePage } from './university-profile.page';

const routes: Routes = [
  {
    path: '',
    component: UniversityProfilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UniversityProfilePageRoutingModule {}
