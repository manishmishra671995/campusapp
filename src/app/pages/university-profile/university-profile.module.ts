import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UniversityProfilePageRoutingModule } from './university-profile-routing.module';

import { UniversityProfilePage } from './university-profile.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UniversityProfilePageRoutingModule
  ],
  declarations: [UniversityProfilePage]
})
export class UniversityProfilePageModule {}
