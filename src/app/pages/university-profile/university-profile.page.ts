import {Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef} from '@angular/core';
import {UserService} from '../../services';
import {ActivatedRoute} from '@angular/router';
import { Messages } from '../../constants/constants';
import {HttpService, HelperService} from '../../services/index';
import {NavController} from '@ionic/angular';
import {Router} from '@angular/router';

@Component({
    selector: 'university-profile',
    templateUrl: './university-profile.page.html',
    styleUrls: ['./university-profile.page.scss'],
})
export class UniversityProfilePage implements OnInit {
    @ViewChild('map', {static: false}) mapElement: ElementRef;
    map:any;
    universityData: any = {
        'latitude': '42.47509330',
        'longitude': '-83.26407550',
        'logo': '',
        'university_images': [
            {

                'image': '',
            }
        ]
    };
    universityId: any;
    universityItem: any;
    universityItemId: any;
    address:any;
    universitySubTypeImge:any;
    universitySubTypeName:any;
    universitySubTypeContacts:any;
    universitySubTypeState:any;
    universitySubTypeCoountry:any;
    newMessage:any;
    constructor(public userService: UserService,
                private httpService: HttpService,
                private router: Router,
                private navCtrl: NavController,
                private helperService: HelperService,
                private route: ActivatedRoute) {
      /*  this.route.params.subscribe(params => {
            console.log("params==>",params);
            this.universityId = params['universityId'];
            this.universityItemId = params['id'];
            this.universityItem = params['university_item'];
            this.getUniversityDetail(this.universityId);
        });*/
        this.universityId = this.route.snapshot.paramMap.get('universityId');
        this.universityItemId = this.route.snapshot.paramMap.get('id');
        this.universityItem = this.route.snapshot.paramMap.get('university_item');
        this.getUniversityDetail(this.universityId);
        this.setUniversityItem(this.universityItem,this.universityItemId,this.universityId );

    }


    async getUniversityDetail(universityId) {
        await this.userService.universityProfile(universityId).then((response) => {
            this.universityData = response['data'];
            console.log(this.universityData);
            let lat = this.universityData.latitude;
            let lng = this.universityData.longitude;
            let university_name = this.universityData['university_name'];
            this.displayGoogleMap(lat, lng);
            this.addMarkersToMap(lat, lng, university_name);
        });
    }

    async setUniversityItem(universityItem,universityItemId,universityId){
        console.log(universityItem,universityItemId,universityId);
        let endPoints='';

        switch (universityItem) {
            case 'group': {
                endPoints='university/group_show/'+universityId+'/'+universityItemId;
                this.userService.universityProfileItem(endPoints).then((response) => {
                    console.log("response==->",response);
                    this.universitySubTypeImge=response['data']['group_logo'];
                    this.universitySubTypeName=response['data']['group_name'];
                    this.universitySubTypeContacts=response['data']['social_links'];
                    this.address=response['data']['address'];
                    this.universitySubTypeState=response['data']['state'];
                    this.universitySubTypeCoountry=response['data']['country'];
                    this.displayGoogleMap(response['data']['latitude'], response['data']['longitude']);
                    this.addMarkersToMap(response['data']['latitude'], response['data']['longitude'], response['data']['group_name']);
                });
                 break;
            }
            case 'contact': {
                endPoints='university/student_contact_show/'+universityId+'/'+universityItemId;
                this.userService.universityProfileItem(endPoints).then((response) => {
                    console.log("response==->",response);
                    this.universitySubTypeImge=response['data']['student_pic'];
                    this.universitySubTypeName=response['data']['student_name'];
                    this.universitySubTypeContacts=response['data']['social_links'];
                    this.address=response['data']['address'];
                    this.universitySubTypeState=response['data']['state'];
                    this.universitySubTypeCoountry=response['data']['country'];
                    this.displayGoogleMap(response['data']['latitude'], response['data']['longitude']);
                    this.addMarkersToMap(response['data']['latitude'], response['data']['longitude'], response['data']['student_name']);
                });
                break;
            }
            case 'department': {
                endPoints='university/department_show/'+universityId+'/'+universityItemId;
                this.userService.universityProfileItem(endPoints).then((response) => {
                    this.universitySubTypeImge=response['data']['department_logo'];
                    this.universitySubTypeName=response['data']['department_name'];
                    this.universitySubTypeContacts=response['data']['social_links'];
                    this.address=response['data']['address'];
                    this.universitySubTypeState=response['data']['state'];
                    this.universitySubTypeCoountry=response['data']['country'];
                    this.displayGoogleMap(response['data']['latitude'], response['data']['longitude']);
                    this.addMarkersToMap(response['data']['latitude'], response['data']['longitude'], response['data']['department_name']);
                    console.log("response==->",response);
                });

                break;
            }
            default:{


            }
        }



    }

    ngOnInit() {
    this.newMessage = localStorage.getItem('campus-app-chat-message');
    }

    displayGoogleMap(lat, lng) {
        console.log('lat,lng', lat, lng);
        const latLng = new google.maps.LatLng(lat, lng);

        const mapOptions = {
            center: latLng,
            disableDefaultUI: true,
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    }


    addMarkersToMap(lat, lng, university_name) {

        const position = new google.maps.LatLng(lat, lng);


        const universityProfile = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: position,
            icon: {
                url: '../../assets/icon/red-dot.png'
            },
            title: university_name,
        });
        universityProfile.setMap(this.map);
    }


    redirectToPage(page) {
        this.router.navigateByUrl('/'+page);
    }

    redirectToNearbyAppartents() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
        }
    }
    openUniversityPage() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
        }
    }

    openChat() {
        this.router.navigateByUrl('/chatlist');
    }

    searchuniversity() {
        this.router.navigateByUrl('/homesearch');
    }
    goBack() {
      this.navCtrl.back();
    }

    openInfo(url) {
        console.log('url', url);
        window.open(url, '_blank');
    }

}
