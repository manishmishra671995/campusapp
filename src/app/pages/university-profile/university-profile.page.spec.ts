import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UniversityProfilePage } from './university-profile.page';

describe('UniversityProfilePage', () => {
  let component: UniversityProfilePage;
  let fixture: ComponentFixture<UniversityProfilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniversityProfilePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UniversityProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
