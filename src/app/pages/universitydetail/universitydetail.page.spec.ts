import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UniversitydetailPage } from './universitydetail.page';

describe('UniversitydetailPage', () => {
  let component: UniversitydetailPage;
  let fixture: ComponentFixture<UniversitydetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniversitydetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UniversitydetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
