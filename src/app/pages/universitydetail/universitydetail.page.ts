import {Component, OnInit, ChangeDetectorRef} from '@angular/core';
import {HelperService, HttpService, UserService} from '../../services';
import {Router} from '@angular/router';
import {Messages} from '../../constants/constants';
import {Events, NavController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {NavigationExtras} from '@angular/router';


@Component({
    selector: 'universitydetail',
    templateUrl: './universitydetail.page.html',
    styleUrls: ['./universitydetail.page.scss'],
})
export class UniversitydetailPage implements OnInit {

    universityId;

    universityData: any = {
        'latitude': '42.47509330',
        'longitude': '-83.26407550',
        'logo': '',
        'is_following': '',
        'university_images': [
            {

                'image': '',
            }
        ],
        'university_fees': [
            {
                'fees_amount': '',
                'fees_unit': ''
            }
        ],

        'university_credits': [
            {
                'credit_from': '',
                'credit_to': '',
                'credit_tag': ''
            }
        ],
        'university_intakes': [
            {
                'id': '',
                'university_id': '',
                'intake_name': '',
                'intake_link': ''
            }
        ],

        'university_weather': [
            {
                'id': '',
                'university_id': '',
                'month_from': '',
                'month_to': '',
                'weather_type': '',
                'weather_link': ''
            }
        ],
        'university_groups': [
            {
                'id': '',
                'university_id': '',
                'group_name': '',
                'group_logo': '',
                'address': '',
                'latitude': '',
                'longitude': ''
            }
        ],
        'university_students_contact': [
            {
                'id': '',
                'university_id': '',
                'student_name': '',
                'student_pic': '',
                'address': '',
                'latitude': '',
                'longitude': ''
            }
        ],
        'university_departments': [
            {
                'id': '',
                'university_id': '',
                'department_name': '',
                'department_logo': '',
                'address': '',
                'latitude': '',
                'longitude': '',
            }
        ],
        'university_image': []
    };

    weatherImages: any = ['../assets/icon/snow-icon.png', '../assets/icon/sun-icon.png', '../assets/icon/rain-icon.png', '../assets/icon/snow-icon.png'];
    newMessage:any;
    constructor(public userService: UserService,
                private httpService: HttpService,
                private events: Events,
                private navCtrl: NavController,
                private changeRef: ChangeDetectorRef,
                private helperService: HelperService,
                private route: ActivatedRoute,
                private router: Router) {

        this.universityId = this.route.snapshot.paramMap.get('id');
        this.getUniversityDetail(this.universityId);

    }


    ionViewWillEnter() {
    }


    followUniversity(id, condition, changeTo) {
        let data = {'university_id': id};
        let apiUrl = 'university/' + condition;
        this.httpService.post(apiUrl, data)
            .subscribe(
                (response: any) => {
                    console.log('data', response);
                    this.universityData.is_following = changeTo;
                    this.changeRef.detectChanges();
                    this.events.publish('followUnfollowUniversity', {'id': id, 'follow': condition});
                },
                (err: any) => {
                    console.log(err);
                    throw(err);

                });
    }

    openInfo(url) {
        console.log('url', url);
        window.open(url, '_blank');
    }

    async getUniversityDetail(id) {

        await this.userService.universityProfile(id).then((response) => {
            this.universityData = response['data'];
            this.changeRef.detectChanges();

            console.log(this.universityData);
        });
    }

    searchuniversity() {
        this.router.navigateByUrl('/homesearch');
    }

    ngOnInit() {
        this.newMessage = localStorage.getItem('campus-app-chat-message');
        this.events.publish('feedbackPopup');
    }

    redirectToPage(page) {
        this.router.navigateByUrl('/' + page);
    }

    redirectToNearbyAppartents() {
        if (localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
        }
    }

    openUniversityPage() {
        if (localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
        }
    }

    goUniversityProfile(university_item, id) {

        this.router.navigate(['university-profile',university_item,id,this.universityId]);

    }


    openChat() {
        this.router.navigateByUrl('/chatlist');
    }

    reviewUniversity() {
        let url = '/universityreview/university/' + this.universityId;
        this.router.navigateByUrl(url);
    }

    goBack() {
      this.navCtrl.back();
    }

    convertToInteger(value) {
        if(value) {
            return parseInt(value);
        }
    }
}
