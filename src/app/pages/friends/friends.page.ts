import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {HttpService, HelperService} from '../../services/index';
import {LoadingController, NavController} from '@ionic/angular';
import { Messages } from '../../constants/constants';
import { Router } from '@angular/router';
@Component({
  selector: 'friends',
  templateUrl: './friends.page.html',
  styleUrls: ['./friends.page.scss'],
})
export class FriendsPage implements OnInit {
	userFriends:any;
	userIgnoredFriends:any;
	friendsTypeToShow:string = 'all';
  noFriendsFound:boolean = false;
  newMessage:any;
  	constructor(private httpService: HttpService,
			  private loading: LoadingController,
      		  private changeRef: ChangeDetectorRef,
    		  private router: Router,
          private navCtrl: NavController,
    		  private helperService: HelperService,) { 
  	}

  	ngOnInit() {
      this.newMessage = localStorage.getItem('campus-app-chat-message');
  		this.getFriends();
  		this.getIgnoredFriends();
  	}
  	getFriends() {
  		this.httpService.get('user/get_friends_list')
        .subscribe(
            (data: any) => {
                console.log('friend', data.data);
                this.userFriends = data.data;
                this.noFriendsFound = true;
                this.changeRef.detectChanges();
                // this.loader.dismiss();
                                    
            },
            (err: any) => {
                // this.loader.dismiss();
                this.changeRef.detectChanges();
                throw(err);
            });
  	}
  	getIgnoredFriends() {
  		this.httpService.get('user/get_ignore_list')
        .subscribe(
            (data: any) => {
                console.log('ignored', data.data);
                this.userIgnoredFriends = data.data;
                this.changeRef.detectChanges();
                // this.loader.dismiss();                                    
            },
            (err: any) => {
                // this.loader.dismiss();
                throw(err);
            });
  	}

  	friendsToShow(type) {
  		this.friendsTypeToShow = type;
  	}

  	searchuniversity() {
	  	this.router.navigateByUrl('/homesearch');
  	}
	  openChat() {
      	this.router.navigateByUrl('/chatlist');
    }

    redirectToPage(page) {
        this.router.navigateByUrl('/'+page);
    }

    redirectToNearbyAppartents() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
        }
    }
    openUniversityPage() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
        }
    }
    pageTobeLoaded(navPageName, id) {
        // this.router.navigateByUrl(navPageName +'/'+ id);
        this.router.navigate([navPageName, id]);
    }
    goBack() {
      this.navCtrl.back();
    }

}
