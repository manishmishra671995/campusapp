import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {HttpService, HelperService} from '../../services/index';
import {LoadingController, NavController} from '@ionic/angular';
import { Messages } from '../../constants/constants';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'mutualfriends',
  templateUrl: './mutualfriends.page.html',
  styleUrls: ['./mutualfriends.page.scss'],
})
export class MutualfriendsPage implements OnInit {
	private loader: any;
	mutualFriendsList:any;
	friendId:any;
	mutualFriendsUnavailable:boolean = false;
  newMessage:any;
  	constructor(private loading: LoadingController,
  				private httpService: HttpService,
  				private route: ActivatedRoute,
          private navCtrl: NavController,
      		  	private changeRef: ChangeDetectorRef,
    		  	private router: Router,
    		  	private helperService: HelperService,) { }

  	ngOnInit() {
      this.newMessage = localStorage.getItem('campus-app-chat-message');
  		this.friendId = this.route.snapshot.paramMap.get('id');
  		this.getMutualFriends();
  	}
  	async getMutualFriends() {
  		this.loader = await this.loading.create({
            message: 'Please wait...'
        });

        await this.loader.present();
  		this.httpService.get('user/get_mutual_friends/'+this.friendId)
	    .subscribe(
	        (data: any) => {
	            console.log('friend', data.data);
	            this.mutualFriendsList = data.data;
              console.log(this.mutualFriendsList)
	            if(!data.data.length) {
	            	this.mutualFriendsUnavailable = true;
	            }
	            this.changeRef.detectChanges();
	            this.loader.dismiss();
	                                
	        },
	        (err: any) => {
	            this.loader.dismiss();
	            throw(err);
	        });
  	}

  	searchuniversity() {
	  	this.router.navigateByUrl('/homesearch');
  	}
	openChat() {
      	this.router.navigateByUrl('/chatlist');
    }

    redirectToPage(page) {
        this.router.navigateByUrl('/'+page);
    }

    redirectToNearbyAppartents() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
        }
    }
    openUniversityPage() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
        }
    }
    goBack() {
      this.navCtrl.back();
    }

}
