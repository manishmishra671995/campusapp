import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MutualfriendsPageRoutingModule } from './mutualfriends-routing.module';

import { MutualfriendsPage } from './mutualfriends.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MutualfriendsPageRoutingModule
  ],
  declarations: [MutualfriendsPage]
})
export class MutualfriendsPageModule {}
