import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MutualfriendsPage } from './mutualfriends.page';

describe('MutualfriendsPage', () => {
  let component: MutualfriendsPage;
  let fixture: ComponentFixture<MutualfriendsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MutualfriendsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MutualfriendsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
