import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {Events, LoadingController} from '@ionic/angular';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { CustomValidators } from '../../validators/custom-validators';
import { HttpService, HelperService, UserService } from '../../services/index';
import {ENV} from '../../environment';
import { Messages } from '../../constants/constants';
import {Router} from '@angular/router';
@Component({
  selector: 'forgetpassword',
  templateUrl: './forgetpassword.page.html',
  styleUrls: ['./forgetpassword.page.scss'],
})
export class ForgetpasswordPage implements OnInit {
	passwordNotSent: boolean = true;
  	passwordResetForm: FormGroup;
  	blankEmail: boolean = false;
  	invalidEmail: boolean = false;
  	passwordEmail = {email: ""};
  	otp1: any;
  	otp2: any;
  	otp3: any;
  	otp4: any;
  	otp5: any;
  	loader: any;
  	constructor(
    	private httpService: HttpService,
    	private helperService: HelperService,
      private changeRef: ChangeDetectorRef,
    	private navCtrl: NavController,
      private userService: UserService,
      public router: Router,
    	private loading: LoadingController) { }

  ngOnInit() {
  	// this.passwordResetForm = this.formBuilder.group({
   //    email: [this.passwordEmail.email, Validators.compose([Validators.required, CustomValidators.email])]
   //  });
  }

  moveFirstFocus(nextElement, event, currElement) {
    let otpVal = '';
    if(event == '') {
      return;
    }
    otpVal = event
    if(otpVal.length > 5) {
      this.helperService.showToast('Invalid otp.', 2000, 'bottom');
      event = '';
      this.otp1 = '';
      currElement.value = '';
      this.changeRef.detectChanges();
      return;
    }
    if(otpVal.length > 1 && otpVal.length < 5) {
      otpVal = event.split('')[0];
      event = otpVal;
    }
    if(otpVal.length == 5) {
      this.otp1 = otpVal[0];
      this.otp2 = otpVal[1];
      this.otp3 = otpVal[2];
      this.otp4 = otpVal[3];
      this.otp5 = otpVal[4];
      this.changeRef.detectChanges();
    } else {
      if(otpVal && nextElement && otpVal != ' ') {
        nextElement.setFocus();
      }
    }
    this.checkPassowrdLength();
  }
  moveFocus(nextElement, event) {
    let otpVal = '';
    if(event.target.value.length > 1 ) {
      otpVal = event.target.value.split('');
    }  else {
      otpVal = event.target.value;
    }
    if(otpVal.length > 5) {
      this.helperService.showToast('Invalid otp.', 2000, 'bottom');
      event.target.value = '';
      return;
    }
    if(otpVal.length > 1 && otpVal.length < 5) {
      otpVal = event.target.value.split('')[0];
      event.target.value = otpVal;
    }
    if(otpVal.length == 5) {
      this.otp1 = otpVal[0];
      this.otp2 = otpVal[1];
      this.otp3 = otpVal[2];
      this.otp4 = otpVal[3];
      this.otp5 = otpVal[4];
      this.changeRef.detectChanges();
    } else {
      if(event.keyCode != '8' && nextElement && event.target.value != '') {
        nextElement.setFocus();
      }
    }
    this.checkPassowrdLength();
  }

  checkPassowrdLength() {
    setTimeout (() => {
          if(this.otp1 && this.otp2 && this.otp3 && this.otp4 && this.otp5) {
            let password = this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5;
            if(password.length == 5) {
                this.passwordEntered(password);
              }
          }
    }, 300);
    
  }

  passwordEntered(otp) {
    console.log('password', otp);
    this.verifyOtp(otp)
    // let forgotPassObj = {'email': this.passwordEmail.email, 'otp': otp, 'type': 'forgotPassword'}
    // this.userService.saveForgotPasswordData(forgotPassObj);
    // this.router.navigateByUrl('/createpassword');
  }

  async verifyOtp(enteredOtp) {
    if(this.loader) {
      this.loader.dismiss();
    }
    this.loader = await this.loading.create({
        message: Messages.loadingText
    });
    await this.loader.present();
    this.httpService.post('auth/forgot_password_verify_otp', {email: this.passwordEmail.email, otp:enteredOtp}).subscribe(
      (data:any) => {
        this.loader.dismiss();
        let forgotPassObj = {'email': this.passwordEmail.email, 'otp': enteredOtp, 'type': 'forgotPassword'}
        this.userService.saveForgotPasswordData(forgotPassObj);
        this.router.navigateByUrl('/createpassword');
      },
      (err:any) => {
        this.loader.dismiss();
        console.log(err.status);
        if(err.status === 422) {
          this.helperService.showToast('Invalid Otp', 2000, 'bottom');
        } else {
          throw (err);
        }
    });
  }

  async  sendPassword(email) {
  	if(this.passwordEmail.email == '') {
  		this.blankEmail = true;
  		return;
  	} else {
  		this.blankEmail = false;
  	}
  	if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.passwordEmail.email)) {
  		this.invalidEmail = false;
  	} else {
  		this.invalidEmail = true;
  		return;
  	}
     this.loader = await this.loading.create({
        message: Messages.loadingText
    });
    await this.loader.present();
    this.httpService.post('auth/forgot_password', {email: this.passwordEmail.email}).subscribe(
      (data:any) => {
        this.loader.dismiss();
        console.log('done')
        this.passwordNotSent = false;
        // this.helperService.showToast(data.message, 2000, 'bottom');
      },
      (err:any) => {
        this.loader.dismiss();
        throw (err);
    });
  }

  goBack() {
    this.navCtrl.back();
  }

}
