import {Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LoadingController, Events} from '@ionic/angular';
import { Messages } from '../../constants/constants';
import {HttpService, HelperService} from '../../services/index';
import {Router} from '@angular/router';


@Component({
    selector: 'universitylist',
    templateUrl: './universitylist.page.html',
    styleUrls: ['./universitylist.page.scss'],
})


export class UniversitylistPage implements OnInit {
    @ViewChild('map', {static: false}) mapElement: ElementRef;
    map: any;
    nearByApartmentsActive: boolean = true;
    universityId: any;
    universityData: any;
    appartmentsList: any;
    offCampus: boolean = false;
    onCampus: boolean = false;
    appartmentsOnCampus = [];
    appartmentsOffCampus = [];
    loader: any;
    newMessage:any;
    constructor(private route: ActivatedRoute,
                private httpService: HttpService,
                private loading: LoadingController,
                private router: Router,
                private events: Events,
                private helperService: HelperService,
                private changeRef: ChangeDetectorRef) {
        this.universityId = this.route.snapshot.paramMap.get('id');
        this.getUniversityInfo(this.universityId);
    }

    ngOnInit() {
        this.newMessage = localStorage.getItem('campus-app-chat-message');
        // this.loadMap();
        this.events.publish('feedbackPopup');

    }

    pageTobeLoaded(navPageName, id) {
        // this.router.navigateByUrl(navPageName +'/'+ id);
        this.router.navigate([navPageName, id]);
    }

    getUniversityInfo(id) {
        const url = 'university/show/' + id;
        this.httpService.get(url)
        .subscribe(
            (data: any) => {
                // this.loader.dismiss();
                this.universityData = data.data;
                let lat = this.universityData.latitude;
                let lng = this.universityData.longitude;
                this.displayGoogleMap(lat, lng,this.universityData['university_name']);
                this.getNearByApartments(this.universityId);
                this.changeRef.detectChanges();
                console.log('univerityData', this.universityData);
            },
            (err: any) => {
                // this.loader.dismiss();
                console.log(err);
                throw(err);
                // this.helperService.showToast(errors.message, 2000, 'bottom');
            });
    }


    displayGoogleMap(lat, lng,university_name) {
        console.log('lat,lng', lat, lng);
        const latLng = new google.maps.LatLng(lat, lng);
        const mapOptions = {
            center: latLng,
            disableDefaultUI: true,
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

        this.setUniversityMarker(lat, lng,university_name);
    }


    setUniversityMarker(lat, lng,university_name) {

        const position = new google.maps.LatLng(lat, lng);


        const university = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position:position,
            icon: {
                url: '../../assets/icon/campus-marker.png'
            },
            title: university_name,
        });
        university.setMap(this.map);
    }

    getMarkers(appartmentsList) {
        // tslint:disable-next-line:variable-name
        for (let _i = 0; _i < appartmentsList.length; _i++) {
            if(appartmentsList[_i].apartment.apartment_type != '1') {
                this.addMarkersToMap(appartmentsList[_i]);
            }
        }
    }



    addMarkersToMap(appartmentsList) {
        console.log('appartmentsList.apartment.latitude', appartmentsList.apartment.latitude);

        const position = new google.maps.LatLng(appartmentsList.apartment.latitude, appartmentsList.apartment.longitude);


        const apartmentList = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position:position,
            icon: {
                url: "../../assets/icon/red-dot.png"
            },
            title: appartmentsList.apartment.apartment_name+ '_id_'+appartmentsList.apartment.id,
        });
        this.addInfoWindow(apartmentList, appartmentsList.apartment.latitude);
        apartmentList.setMap(this.map);
    }
    /*map event listener*/
    addInfoWindow(marker, content) {
        google.maps.event.addListener(marker, 'click', () => {
          let appartmentInfo = marker.title.split('_id_');
          let apartmentId = appartmentInfo[1];
          this.scrollToAppartment(apartmentId);
        });
    }
    /*map event listener*/

    scrollToAppartment(id) {
        let appartmentClassName = 'apartment_'+id
        let scrollToAppartment = document.getElementById(appartmentClassName);
        scrollToAppartment.scrollIntoView({ behavior: "smooth" })
    }

    async getNearByApartments(id) {
        this.loader = await this.loading.create({
            message: 'Please wait...'
        });

        await this.loader.present();
        let appartmentNearByUrl = 'university/get_apartments_by_university/' + id;
        this.httpService.get(appartmentNearByUrl)
            .subscribe(
                (data: any) => {
                    this.appartmentsList = data.data;

                    console.log('this.appartmentsList', this.appartmentsList);

                    for (var i = 0; i < this.appartmentsList.length; i++) {
                        if (this.appartmentsList[i].apartment.apartment_type === 1) {

                            this.appartmentsOnCampus.push(this.appartmentsList[i].apartment);
                            console.log('appartmentsOnCampus',  this.appartmentsOnCampus);
                        }

                        if (this.appartmentsList[i].apartment.apartment_type === 2) {
                            this.appartmentsOffCampus.push(this.appartmentsList[i].apartment);
                            console.log('appartmentsOffCampus', this.appartmentsOffCampus);
                        }
                    }

                    if (this.appartmentsOnCampus.length > 0) {
                        this.onCampus = false;
                    }
                    if (this.appartmentsOffCampus.length > 0) {
                        this.offCampus = false;
                    }
                    //this.loadAppartmentsOnMap();
                    // this.changeRef.detectChanges();
                    this.loader.dismiss();

                    let lat = this.appartmentsList[0].apartment.latitude;
                    let lng = this.appartmentsList[0].apartment.longitude;

                    this.getMarkers(this.appartmentsList);
                    // this.changeRef.detectChanges();

                },
                (err: any) => {
                    this.loader.dismiss();
                    throw(err);
                });

    }

    toggleAccordion(accordian) {
        console.log("this.offCampus",this.offCampus);
        if (accordian === 'oncampus') {
            if (this.onCampus === true) {
                this.onCampus = false;
            } else {
                this.offCampus = false;
                this.onCampus = true;
            }
        }
        else if (accordian === 'offcampus') {
            if (this.offCampus === true) {
                this.offCampus = false;
            } else {
                this.offCampus = true;
                this.onCampus = false;
            }
        }
        this.changeRef.detectChanges();
    }

    searchuniversity() {
        this.router.navigateByUrl('/homesearch');
    }

    redirectToPage(page) {
        this.router.navigateByUrl('/'+page);
    }

    redirectToNearbyAppartents() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitylist', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityForApartment, 2000, 'bottom');
        }
    }
    openUniversityPage() {
        if(localStorage.getItem('campus-app-following-university-id')) {
            this.router.navigate(['universitydetail', localStorage.getItem('campus-app-following-university-id')]);
        } else {
            this.helperService.showToast(Messages.selectUniversityMessage, 2000, 'bottom');
        }
    }
    openChat() {
      this.router.navigateByUrl('/chatlist');
    }

    redirectToOnCampus(id) {
        let url = 'apartmentdetail/'+id
        this.router.navigateByUrl(url);
    }
}
