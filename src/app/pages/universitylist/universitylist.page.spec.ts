import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UniversitylistPage } from './universitylist.page';

describe('UniversitylistPage', () => {
  let component: UniversitylistPage;
  let fixture: ComponentFixture<UniversitylistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniversitylistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UniversitylistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
