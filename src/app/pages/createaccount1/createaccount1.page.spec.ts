import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Createaccount1Page } from './createaccount1.page';

describe('Createaccount1Page', () => {
  let component: Createaccount1Page;
  let fixture: ComponentFixture<Createaccount1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Createaccount1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Createaccount1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
