import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from '../../validators/custom-validators';
import { UserService } from '../../services/index';
@Component({
  selector: 'createaccount1',
  templateUrl: './createaccount1.page.html',
  styleUrls: ['./createaccount1.page.scss'],
})
export class Createaccount1Page implements OnInit {
	createAccount1Form: FormGroup;
  	userData = {fname:'', lname:''};
  	constructor(private navCtrl: NavController,
	  			private userService: UserService,
	  			public formBuilder: FormBuilder) { 

  	}

 	ngOnInit() {
 		this.createAccount1Form = this.formBuilder.group({
	       fname: [this.userData.fname, Validators.compose([Validators.required])],
	       lname: [this.userData.lname, Validators.compose([Validators.required])]
	    });
  	}

  	goBack() {
	    this.navCtrl.navigateRoot('/welcome');
  	}

  	createaccount(){
	    console.log(this.userData);
	    this.userService.saveNewUserObj(this.userData);
	    this.navCtrl.navigateRoot('/createaccount2');
  	}

}
