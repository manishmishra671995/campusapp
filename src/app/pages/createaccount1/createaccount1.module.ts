import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Createaccount1PageRoutingModule } from './createaccount1-routing.module';

import { Createaccount1Page } from './createaccount1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    Createaccount1PageRoutingModule
  ],
  declarations: [Createaccount1Page]
})
export class Createaccount1PageModule {}
