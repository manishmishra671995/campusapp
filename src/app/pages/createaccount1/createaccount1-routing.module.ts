import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Createaccount1Page } from './createaccount1.page';

const routes: Routes = [
  {
    path: '',
    component: Createaccount1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Createaccount1PageRoutingModule {}
