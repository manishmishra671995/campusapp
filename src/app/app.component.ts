import {Component, OnInit, ViewEncapsulation, ChangeDetectorRef} from '@angular/core';
import {Router} from '@angular/router';
import {SwUpdate} from '@angular/service-worker';

import {AlertController, Events, LoadingController, MenuController, Platform, ToastController} from '@ionic/angular';

import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {UserData} from './providers/user-data';
import {Storage} from '@ionic/storage';
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook/ngx';
import {Screenshot} from '@ionic-native/screenshot/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
//import {UserData} from './providers/user-data';
import {Push, PushObject, PushOptions} from '@ionic-native/push/ngx';
import {ENV} from './environment';
import {UserService, HttpService, HelperService} from './services/index';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
    userInfo:any=null;
    school;
    userUniversities;
    deleteUniversities:boolean = false;
    capturedScreenshot;
    feebackText;
    user_profile_pic:any=null;
    ENV_STRING: string = ENV.CURRENT_ENV;
    showFeedBackPopup: boolean = false;
    appPages = [
        {
            title: 'Schedule',
            url: '/app/tabs/schedule',
            icon: 'calendar'
        },
        {
            title: 'Speakers',
            url: '/app/tabs/speakers',
            icon: 'contacts'
        },
        {
            title: 'Map',
            url: '/app/tabs/map',
            icon: 'map'
        },
        {
            title: 'Login',
            url: '/app/login',
            icon: ''
        },
        {
            title: 'About',
            url: '/app/tabs/about',
            icon: 'information-circle'
        }
    ];
    loggedIn = false;
    dark = false;
    onscreen: any;
    state: boolean = false;

    constructor(
        private events: Events,
        private menu: MenuController,
        private platform: Platform,
        private router: Router,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private storage: Storage,
        private userData: UserData,
        private swUpdate: SwUpdate,
        private httpService: HttpService,
        private userService: UserService,
        private screenshot: Screenshot,
        private helperService: HelperService,
        private toastCtrl: ToastController,
        private changeRef: ChangeDetectorRef,
        private push: Push,
        public alertCtrl: AlertController,
        private keyboard: Keyboard,
        private fb: Facebook
    ) {
        this.initializeApp();
    }


    async ngOnInit() {
        console.log('app', this.router.url);
        this.checkLoginStatus();
        this.listenForLoginEvents();

        this.swUpdate.available.subscribe(async res => {
            const toast = await this.toastCtrl.create({
                message: 'Update available!',
                showCloseButton: true,
                position: 'bottom',
                closeButtonText: `Reload`
            });

            await toast.present();

            toast
                .onDidDismiss()
                .then(() => this.swUpdate.activateUpdate())
                .then(() => window.location.reload());
        });

        /*set menu variables*/
        this.events.subscribe('userLoggedIn', (data) => {
            this.menu.enable(true);
            this.userInfo = data;
            if(this.userInfo && this.userInfo.user_profile_picture) {
                this.user_profile_pic = this.userInfo.user_profile_picture.profile_picture;
              } else {
                this.updateUserInfo();
            }
            this.followingUniversity();
            // this.changeRef.detectChanges();

        });

        this.events.subscribe('followUnfollowUniversity', (data) => {

            this.followingUniversity();

        });
        this.events.subscribe('feedbackPopup', () => {

            this.closeFeedback();

        });
        if (localStorage.getItem('campus-app-' + this.ENV_STRING + '-token')) {
            if(localStorage.getItem('campus-app-logged-in-user')) {
                this.userInfo = JSON.parse(localStorage.getItem('campus-app-logged-in-user'));
                if(this.userInfo && this.userInfo.user_profile_picture) {
                    this.user_profile_pic = this.userInfo.user_profile_picture.profile_picture;
                }else{
                    this.setFbProfilePic();
                }
                this.followingUniversity();
            } else {
                this.httpService.get('user/profile_show')
                .subscribe(
                    (data: any) => {
                        this.events.publish('userLoggedIn', data.data);
                        // this.navCtrl.navigateRoot('/adduniversity');
                        // this.changeRef.detectChanges();
                    },
                    (err: any) => {
                        throw(err);
                    });
            }

        }
        /*set menu variables*/
    }

    followingUniversity() {
        this.httpService.get('university/follow_list')
            .subscribe(
                (data: any) => {

                    // TODO check if loadercal storage is available only then set token.Not sure if such a case can occur in mobile.
                    // let test = data;
                    console.log(data, data.data);
                    if (data.data.length) {
                        this.userUniversities = data.data;
                        localStorage.setItem('campus-app-following-university-id', data.data[0].university.id);
                        // localStorage.setItem('campus-app-following university-id', data.data[0].university.id);
                    } else {
                        this.userUniversities = [];
                        localStorage.removeItem('campus-app-following-university-id');
                        localStorage.removeItem('campus-app-following university-id');
                    }
                    this.changeRef.detectChanges();
                },
                (err: any) => {
                    throw(err);
                });
    }

    updateUserInfo() {
        this.httpService.get('user/profile_show')
            .subscribe(
                (data: any) => {
                    localStorage.setItem('campus-app-logged-in-user', JSON.stringify(data.data));
                    this.userInfo = JSON.parse(localStorage.getItem('campus-app-logged-in-user'));
                    if(this.userInfo.user_profile_picture) {
                        this.user_profile_pic = this.userInfo.user_profile_picture.profile_picture;
                    }else{
                        this.setFbProfilePic();
                    }
                    this.changeRef.detectChanges();
                },
            (err: any) => {
                throw(err);
            });
    }

    setFbProfilePic(){
        this.storage.get('facebookUrl').then(value => {
            if (value != null) {
                console.log('val', value);
                if (this.user_profile_pic === null) {
                    this.user_profile_pic = value;
                }
             }
        });
    }


    addUniversity() {
        this.menu.toggle();
        this.router.navigateByUrl('/adduniversity');
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.overlaysWebView(true);
            /*to change height on keyboard*/
            let appEl = <HTMLElement>(document.getElementsByTagName('ION-APP')[0]),
            appElHeight = appEl.clientHeight;

            // Keyboard.disableScroll(true);

            window.addEventListener('native.keyboardshow', (e) => {
                if(this.router.url.indexOf('chat') > -1) {
                    // appEl.style.height = (appElHeight - (<any>e).keyboardHeight) + 'px';
                    let height = (<any>e).keyboardHeight + 25;
                    // let marginBottom = window.innerHeight - height;
                    appEl.style.bottom = height + 'px';
                    let messagesContainer = document.getElementsByClassName('message');
                    if(messagesContainer.length) {
                      let scrollTo = document.getElementsByClassName('message')[messagesContainer.length - 1];
                      scrollTo.scrollIntoView(true)
                    }
                }
                if(this.router.url.indexOf('otpverify') > -1) {
                    // appEl.style.height = (appElHeight - (<any>e).keyboardHeight) + 'px';
                    // alert('otp');
                    let height = (<any>e).keyboardHeight;
                    // let marginBottom = window.innerHeight - height;
                    appEl.style.bottom = height + 'px';
                    let otareaCont = document.getElementById('otp_area');
                    otareaCont.scrollIntoView(true)
                }
                if(this.router.url.indexOf('forgetpassword') > -1) {
                    // appEl.style.height = (appElHeight - (<any>e).keyboardHeight) + 'px';
                    // alert('otp');
                    let height = (<any>e).keyboardHeight;
                    // let marginBottom = window.innerHeight - height;
                    appEl.style.bottom = height + 'px';
                    let otareaCont = document.getElementById('otp_area');
                    otareaCont.scrollIntoView(true)
                }

            });
            window.addEventListener('native.keyboardwillhide', (e) => {
                if(this.router.url.indexOf('chat') > -1) {
                    // alert('yes');
                    this.keyboard.show();
                }

                // if(this.router.url.indexOf('chat') > -1) {
                //     // alert('yes');
                //     this.keyboard.show();
                // }

            });
            window.addEventListener('keyboardWillHide', () => {
                // alert('hide');
            });

            window.addEventListener('native.keyboardhide', () => {
                // appEl.style.height = '100%';
                if(this.router.url.indexOf('chat') > -1) {
                    // appEl.style.height = (appElHeight - (<any>e).keyboardHeight) + 'px';
                    appEl.style.bottom = '0';
                } else {
                    appEl.style.bottom = '0';
                }
            });
            /*to change height on keyboard*/
            // set status bar to white
            this.statusBar.backgroundColorByHexString('#0066cc');
            this.splashScreen.hide();
            this.platform.backButton.subscribe(() => {
                this.closeFeedback();
                // this does work
            });
            this.initPushNotification();
        });
    }


    async initPushNotification() {
        const options: PushOptions = {
            android: {
                senderID: '1070885639941',
                sound: 'true',
                vibrate: true,
                forceShow: '1',
            },
            ios: {
                alert: 'true',
                badge: false,
                sound: 'true'
            },
            windows: {}
        };


        const pushObject: PushObject = this.push.init(options);

        pushObject.on('registration').subscribe((data: any) => {
            console.log('Toek Data', data);
            console.log('device token -> ' + data.registrationId);

            this.storage.set('deviceToken', data.registrationId);

            //TODO - send device token to server
        });

        pushObject.on('notification').subscribe((data: any) => {
            console.log('message -> ' + data.message);
            //if user using app and push notification comes
            if(data.additionalData.notification_type == 'CHAT_MESSAGE') {
                if(this.router.url.indexOf('chat') > -1) {
                    
                } else {
                    localStorage.setItem('campus-app-chat-message', 'true');
                    let chatBtns = (<HTMLScriptElement[]><any>document.getElementsByClassName('chat-btn'));
                    for(let k = 0; k < chatBtns.length; k++) {
                        chatBtns[k].innerHTML += '<span class="unread-msgs"></span>'
                    }
                }
            }
            if (data.additionalData.foreground ) {
                // if application open, show popup
                if(data.additionalData.notification_type != 'CHAT_MESSAGE') {
                    this.openNotifcationAlert(data);
                }
            } else {
                if(this.userInfo!=null)
                 this.router.navigateByUrl('/notification');

                // this.openNotifcationAlert(data);

                //if user NOT using app and push notification comes
                //TODO: Your logic on click of push notification directly
                // this.nav.push(DetailsPage, { message: data.message });
                // console.log('Push notification clicked');
            }
        });

        pushObject.on('error').subscribe(error => alert('Error with Push plugin' + error));
    }

    async openNotifcationAlert(data) {
        const alert = await this.alertCtrl.create({
            header: 'New Notification',
            message: data.message,
            buttons: [{
                text: 'Ignore',
                role: 'cancel'
            }, {
                text: 'View',
                handler: () => {
                    if(this.userInfo!=null)
                        this.router.navigateByUrl('/notification');
                    //this.nav.push(MyNotificationPage);

                    //TODO: Your logic here
                    // this.nav.push(DetailsPage, { message: data.message });
                }
            }]
        });
        await alert.present();
    }

    checkLoginStatus() {
        return this.userData.isLoggedIn().then(loggedIn => {
            return this.updateLoggedInStatus(loggedIn);
        });
    }

    updateLoggedInStatus(loggedIn: boolean) {
        setTimeout(() => {
            this.loggedIn = loggedIn;
        }, 300);
    }

    listenForLoginEvents() {
        this.events.subscribe('userLoggedIn', () => {
            this.updateLoggedInStatus(true);
            this.setFbProfilePic();
        });

        this.events.subscribe('user:signup', () => {
            this.updateLoggedInStatus(true);
        });

        this.events.subscribe('user:logout', () => {
            this.updateLoggedInStatus(false);
        });

        this.events.subscribe('user:updateProfile', () => {
           this.updateUserInfo();
        });
    }

    /*    logout() {
            this.menu.close()
            this.logoutFb();
            this.userData.logout().then(() => {
                // return this.router.navigateByUrl('/app/tabs/schedule');
                return this.router.navigateByUrl('welcome');
            });

        }*/

    logoutFb() {
        this.fb.logout()
            .then(res => {
                console.log('fbLogout', res);
            })
            .catch(e => console.log('Error logout from Facebook', e));
    }

    async logout() {
        const alert = await this.alertCtrl.create({
            header: 'Confirm!',
            message: '<strong>Are You Sure</strong> ?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Logout',
                    handler: () => {
                        this.menu.toggle();
                        this.userService.logout();
                        this.logoutFb();
                    }
                }
            ]
        });
        await alert.present();
    }

    openTutorial() {
        this.menu.enable(false);
        this.storage.set('ion_did_tutorial', false);
        this.router.navigateByUrl('/tutorial');
    }

    showFeedbackPopup() {
        this.menu.toggle();
        setTimeout(() => {
            this.screenShot();
        }, 500);

    }

    displayFeedbackPopup() {
        this.showFeedBackPopup = true;
        this.changeRef.detectChanges();
    }

    closeFeedback() {
        this.feebackText = '';
        this.capturedScreenshot = '';
        this.showFeedBackPopup = false;
        this.changeRef.detectChanges();
    }

    /*screenshot codes*/
    screenreset() {
        var self = this;
        setTimeout(function () {
            self.state = false;
        }, 1000);
    }

    screenShot() {
        this.screenshot.URI(80).then(res => {
            this.capturedScreenshot = '';
            console.log(res.URI);
            this.state = true;
            this.capturedScreenshot = res.URI;
            this.showFeedBackPopup = true;
            this.changeRef.detectChanges();
            this.screenreset();
        });
    }

    /*screenshot codes*/

    saveFeedback() {
        let feedbackData = {'feedback': this.feebackText, 'screenshot': this.capturedScreenshot};
        if (this.feebackText) {
            this.httpService.post('feedback/store', feedbackData)
                .subscribe(
                    (data: any) => {
                        this.showFeedBackPopup = false;
                        this.feebackText = '';
                        this.capturedScreenshot = '';
                        this.changeRef.detectChanges();
                        this.helperService.showToast(data.message, 2000, 'bottom');

                    },
                    (err: any) => {
                        throw(err);
                    });
        } else {
            this.helperService.showToast('Please enter your review to continue.', 2000, 'bottom');
        }
    }

    editOwnProfile() {
        // this.menu.toggle();
        // this.router.navigateByUrl('/profileother');
        this.deleteUniversities = true;
        this.changeRef.detectChanges();
    }
    stopRemovingUniversity() {
        this.deleteUniversities = false;
        this.changeRef.detectChanges();
    }
    adduniversity() {
        this.menu.toggle();
        this.router.navigateByUrl('/adduniversity');
    }

    pageTobeLoaded(navPageName, id) {
        // this.router.navigateByUrl(navPageName +'/'+ id);
        this.menu.toggle();
        this.router.navigate([navPageName, id]);
    }


    goBack() {
        console.log('here')
    }

    setUniversitySelected(id) {
        localStorage.setItem('campus-app-following-university-id', id);
        this.menu.toggle();
    }
    redirectToPage(page) {
        this.menu.toggle();
        this.router.navigateByUrl('/' + page);
    }

    unfollowUniversity(id) {
        let data = {'university_id': id};
        this.httpService.post('university/unfollow', data)
        .subscribe(
            (data: any) => {
                console.log('data', data);
                this.events.publish('followUnfollowUniversity', id);
                // this.searchResults[index].is_following = changeTo;
            },
            (err: any) => {
                console.log(err);
                throw(err);

            });
    }

    imageExpired() {
        this.httpService.get('user/profile_show')
        .subscribe(
            (data: any) => {
                this.events.publish('userLoggedIn', data.data);
                // this.navCtrl.navigateRoot('/adduniversity');
                // this.changeRef.detectChanges();
            },
            (err: any) => {
                throw(err);
            });
    }

}
