export interface UserOptions {
    email: string;
    password: string;
}

export interface UserData {
    grant_type: string;
    social_provider: string;
    social_token: string;
}

