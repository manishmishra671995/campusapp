export class ENV {

  public static CURRENT_ENV: string = 'production';
  // public static CURRENT_ENV: string = 'ngRoklocal';
  // public static CURRENT_ENV: string = 'development';
  //public static CURRENT_ENV: string = 'production';

  public static ENVIRONMENT: any = {
    'local':{
      API_URL:'http://3.213.156.215/api/'
    },
    'development':{
      API_URL:''
    },
    'production': {
      API_URL:'https://login.setlor.com/api/'
    },
    'ngRoklocal':{
      API_URL:''
    }
  };
}