export const Messages = {
error500 : 'Something went wrong. Please try again.',
selectUniversityForApartment: 'Please select university from menu to view nearby apartments.',
selectUniversityMessage: 'Please select university from menu to continue.',
loadingText : 'Please Wait..',
habitsMaster : [ {id: 1, url: '../assets/icon/na-icon.png',  text: 'n/a'},
				 {id: 2, url: '../assets/icon/meat-icon.png',  text: 'meat'},
				 {id: 3, url: '../assets/icon/nomeat-icon.png',  text: 'no meat'},
				 {id: 4, url: '../assets/icon/na-icon.png',  text: 'n/a'},
				 {id: 5, url: '../assets/icon/smoking-icon.png',  text: 'smokes'},
				 {id: 6, url: '../assets/icon/nosmoking-icon.png',  text: 'doesnt smoke'},
				 {id: 7, url: '../assets/icon/na-icon.png',  text: 'n/a'},
				 {id: 8, url: '../assets/icon/drinks-icon.png',  text: 'drinks'},
				 {id: 9, url: '../assets/icon/nodrinks-icon.png',  text: 'doesnt drink'},
				 {id: 10, url: '../assets/icon/na-icon.png',  text: 'n/a'},
				 {id: 11, url: '../assets/icon/moon-icon.png',  text: 'night owl'},
				 {id: 12, url: '../assets/icon/sun-icon.png',  text: 'early bird'}	],
X_Long : 0,

Y_Latt: 0,

StdDev: 1,
}