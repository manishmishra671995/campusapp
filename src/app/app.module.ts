import {HttpClientModule} from '@angular/common/http';
import {NgModule, ErrorHandler} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BrowserModule, HAMMER_GESTURE_CONFIG} from '@angular/platform-browser';
import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {IonicModule} from '@ionic/angular';
import { Toast } from '@ionic-native/toast/ngx';
import { GoogleMaps } from '@ionic-native/google-maps/ngx';
//Modules
import {CustExtBrowserXhr} from './app.xhr';

import {Facebook} from '@ionic-native/facebook/ngx';
import {GooglePlus} from '@ionic-native/google-plus/ngx';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {HttpService, UserService, HelperService, AutoCompleteService, StateAutoCompleteService} from './services/index';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Screenshot } from '@ionic-native/screenshot/ngx';

//Custom Error Handler
import {CustErrorHandler} from './app.custErrHandler';
import {FilterPipeModule} from 'ngx-filter-pipe';
import {IonicStorageModule} from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AutoCompleteModule } from 'ionic4-auto-complete';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import {IonicGestureConfig} from './providers/ionic-gesture-config';
const config: SocketIoConfig = { url: 'https://login.setlor.com:9000', options: {} };

@NgModule({
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        //IonicModule.forRoot(),
        IonicModule.forRoot({ scrollAssist: true, scrollPadding: true }),
        IonicStorageModule.forRoot(),
        FilterPipeModule,
        AutoCompleteModule,
        SocketIoModule.forRoot(config),
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: environment.production
        })
    ],
    declarations: [AppComponent],
    providers: [
        InAppBrowser,
        SplashScreen,
        StatusBar,
        Facebook,
        Screenshot,
        Toast,
        GooglePlus,
        HttpService,
        AutoCompleteService,
        StateAutoCompleteService,
        UserService,
        GoogleMaps,
        Camera,
        GoogleMaps,
        Geolocation,
        Keyboard,
        {provide: ErrorHandler, useClass: CustErrorHandler},
        {
            provide: HAMMER_GESTURE_CONFIG,
            useClass: IonicGestureConfig
        },
        Push,
        HelperService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
