import { Injectable } from "@angular/core";
import { Toast } from '@ionic-native/toast/ngx';

@Injectable()

export class HelperService {

  constructor(
    private toast: Toast) {
  }

  showToast(msg, duration, position) {
    (<any>window).cordova && this.toast.show(msg, duration, position).subscribe(
      toast => {
        
      }
    );
  }
}

