import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {LoadingController} from '@ionic/angular';
import {map} from 'rxjs/operators';
import {ENV} from '../environment';
import {Observable} from 'rxjs';


@Injectable()

export class HttpService {
    loader: any;

    constructor(
        private http: HttpClient,
        private loading: LoadingController) {
    }

    API_URL: any = ENV.ENVIRONMENT[ENV.CURRENT_ENV].API_URL;
    ENV_STRING: string = ENV.CURRENT_ENV;

    post(url, data) {
        let headers;
        if (localStorage.getItem('campus-app-' + this.ENV_STRING + '-token')) {
            headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('campus-app-' + this.ENV_STRING + '-token'));
        }
        return this.http.post(
            this.API_URL + url,
            data,
            {headers}
        ).pipe(map(res => res,
        ));
    }

    postLogin(url, data) {
        return new Promise((resolve, reject) => {
            let headers;
        if (localStorage.getItem('campus-app-' + this.ENV_STRING + '-token')) {
            headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('campus-app-' + this.ENV_STRING + '-token'));
        }
         this.http.post(
            this.API_URL + url,
            data,
            {headers}
        ).subscribe(res => {
            console.log("res",res);
             resolve(res);
             // alert(JSON.stringify(res));
         }, (err) => {
             console.log("res",err);

             reject(err);
             // alert(err);
         });
        });
    }


    // post(data: Data): Observable<HttpResponse<object>> {
    //   // return this.http.post<RegisterResponse>(this.API_URL + url, data, this.getRequestOptions());
    //   return this.http.post(this.API_URL + url, data, this.getRequestOptions());
    // }

    put(url, data) {
        let headers;
        if (localStorage.getItem('campus-app-' + this.ENV_STRING + '-token')) {
            headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('campus-app-' + this.ENV_STRING + '-token'));
        }
        return this.http.put(
            this.API_URL + url,
            data,
            {headers}
        ).pipe(map(res => res));
    }

    get(url, data?) {
        let headers;
        let params;
        // if(data) {
        //   params = new HttpParams().set(data);
        // }
        if (localStorage.getItem('campus-app-' + this.ENV_STRING + '-token')) {
            headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('campus-app-' + this.ENV_STRING + '-token'));
        }

        return this.http.get(
            url.indexOf(this.API_URL) > -1 ? url : this.API_URL + url,
            {headers}
        ).pipe(map(res => res,
        ));
    }

    delete(url) {
        return this.http.delete(
            this.API_URL + url,
            this.getRequestOptions()
        ).pipe(map(res => res));
    }

    getRequestOptions(params?) {
        let headers = new HttpHeaders();
        headers.set('Content-Type', 'application/json');

        localStorage.getItem('campus-app-' + this.ENV_STRING + '-token') ? headers.set('Authorization', 'Bearer ' + localStorage.getItem('campus-app-' + this.ENV_STRING + '-token')) : '';
        // return new RequestOptions({ headers: headers, params: params });
        let options = {
            headers: headers
        };
        return options;
    }

    // Todo temp func to delete
    gettemp(url, data?) {
        return this.http.get(
            url,
            this.getRequestOptions(data)
        ).pipe(map(res => res,
        ));
    }

}

