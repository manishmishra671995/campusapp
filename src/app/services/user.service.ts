import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import {NavController} from '@ionic/angular';
import {HttpClient} from '@angular/common/http';
import {HttpService} from './http.service';
import {HelperService} from './helper.service';
import {Events, LoadingController} from '@ionic/angular';
import {ENV} from '../../app/environment';
import {Messages} from '../../app/constants/constants';
import 'rxjs/add/operator/map';
import {App} from 'ionic-angular';
import {map} from 'rxjs/operators';
import {Storage} from '@ionic/storage';
import {Router} from '@angular/router';


@Injectable()

export class UserService {
    passwordToken: any;
    userInfo: any;
    LoggedInUserData: any;
    registerUser: any;
    savedUserData: any;
    private loader: any;
    private isAdmin = false;
    private passwordForgetData: any;
    userOnChat: any;
    constructor(private http: HttpClient,
                private events: Events,
                private httpService: HttpService,
                private navCtrl: NavController,
                private helperService: HelperService,
                private loading: LoadingController,
                private router: Router,
                private storage: Storage) {

    }

    ENV_STRING: string = ENV.CURRENT_ENV;

    setChatUser(user) {
        this.userOnChat = user;
    }

    getChatUser() {
        return this.userOnChat;
    }

    savePwdToken(pwdToken) {
        this.passwordToken = pwdToken;
    }

    saveForgotPasswordData(data) {
        this.passwordForgetData = data;
    }

    getForgotPasswordData() {
        if (this.passwordForgetData) {
            return this.passwordForgetData;
        }
    }

    getNewUserObj() {
        return this.registerUser;
    }

    getLoggedInUserData() {
        if (this.LoggedInUserData) {
            return this.LoggedInUserData;
        } else {

        }
    }

    saveNewUserObj(userData) {
        this.registerUser = userData;
    }

    getPwdToken() {
        return this.passwordToken;
    }

    async login(userData) {
        console.log('userData', userData);
        // this.loader = this.loading.create({
        //     content: Messages.loadingText,
        // });

        this.loader = await this.loading.create({
            message: 'Please wait...'
        });


        await this.loader.present();
        this.httpService.postLogin('auth/login', userData)
            .then(
                (data: any) => {

                    // TODO check if loadercal storage is available only then set token.Not sure if such a case can occur in mobile.
                    // let test = data;
                    console.log(data, data.data);
                    this.LoggedInUserData = data.data;
                    localStorage.setItem('campus-app-logged-in-user', JSON.stringify(data.data));
                    localStorage.setItem('campus-app-logged-in-user-id', data.data.id);
                    localStorage.setItem('campus-app-' + this.ENV_STRING + '-token', data.data.token);
                    this.loader.dismiss();
                    this.events.publish('userLoggedIn', data.data);
                    this.getProfileData();
                    if (this.registerUser) {
                        this.navCtrl.navigateRoot('/profile');
                    } else {
                        this.navCtrl.navigateRoot('/adduniversity');
                    }
                    this.helperService.showToast('Logged in successfully.', 2000, 'bottom');
                },

                (err: any) => {
                    console.log('error', err.error.message);
                    this.loader.dismiss();

                    if(err && err.status && err.status == '403') {
                        this.helperService.showToast('Incorrect email or password.', 2000, 'bottom');
                    } else if(err && err.status && err.status == '401' && err.error.data && err.error.data.is_verified == '0') {
                        this.saveNewUserObj({'email': userData.email, 'id': err.error.data.user_id});
                        this.navCtrl.navigateRoot('/otpverify');
                    }
                    else {
                        this.helperService.showToast(err.error.message, 5000, 'bottom');
                    }
                    throw(err);
                });
    }

    async socialLogin(userData) {
        console.log('userData', userData);
        this.loader = await this.loading.create({
            message: 'Please wait...'
        });
        await this.loader.present();
        this.httpService.postLogin('auth/login', userData)
            .then(
                (data: any) => {

                    // TODO check if loadercal storage is available only then set token.Not sure if such a case can occur in mobile.
                    // let test = data;
                    console.log(data, data.data);
                    this.LoggedInUserData = data.data;
                    localStorage.setItem('campus-app-logged-in-user', JSON.stringify(data.data));
                    localStorage.setItem('campus-app-logged-in-user-id', data.data.id);
                    localStorage.setItem('campus-app-' + this.ENV_STRING + '-token', data.data.token);
                    this.loader.dismiss();
                    this.events.publish('userLoggedIn', data.data);
                    this.getProfileData();
                    if (data.data['created_at'] === data.data['updated_at']) {
                        this.navCtrl.navigateRoot('/profile');
                    } else {
                        this.navCtrl.navigateRoot('/adduniversity');
                    }
                    this.helperService.showToast('Logged in successfully.', 2000, 'bottom');
                },

                (err: any) => {
                    console.log('error', err.error.message);
                    this.loader.dismiss();

                    if(err && err.status && err.status == '403') {
                        this.helperService.showToast('Incorrect email or password.', 2000, 'bottom');
                    } else {
                        this.helperService.showToast(err.error.message, 5000, 'bottom');
                    }
                    throw(err);
                });
    }

    async getProfileData() {
     /*   this.loader = await this.loading.create({
            message: 'Please wait...'
        });

        await this.loader.present();*/
        // return new Promise((resolve, reject) => {


        return this.httpService.get('user/profile_create')
            .subscribe(
                (data: any) => {
                    console.log(data);
                    this.loader.dismiss();
                    if (data['status'] === 1) {
                        this.storage.set('getProfile', data.data);
                        // this.navCtrl.navigateRoot('/profile');
                        //resolve(data.data);
                    }
                },
                (err: any) => {
                    this.loader.dismiss();
                    throw(err);
                });
        // });
    }


    async getStateData(stateId) {
        this.loader = await this.loading.create({
            message: 'Please wait...'
        });

        await this.loader.present();
        return new Promise((resolve, reject) => {

            this.httpService.get('user/get_states/'+stateId)
            .subscribe(
                (data: any) => {
                    this.loader.dismiss();

                    console.log(data);
                    resolve(data.data);
                },
                (err: any) => {
                    this.loader.dismiss();

                    throw(err);
                });
         });
    }

    saveProfileInfo(data) {
        this.savedUserData = data;
    }

    getSavedUserInfo() {

        return this.savedUserData;
    }

    async postProfileData(profileData) {
        this.loader = await this.loading.create({
            message: 'Please wait...'
        });
        await this.loader.present();
        return this.httpService.post('user/profile_update', profileData)
            .subscribe(
                (data: any) => {
                    // TODO check if loadercal storage is available only then set token.Not sure if such a case can occur in mobile.
                    // let test = data;
                    console.log(data);
                    this.loader.dismiss();
                    this.router.navigateByUrl('/homesearch');

                },
                (err: any) => {
                    this.loader.dismiss();
                    console.log(err);
                    // this.helperService.showToast(errors.message, 2000, 'bottom');
                });
    }

    async getOther_profile_show(id) {
        this.loader = await this.loading.create({
            message: 'Please wait...'
        });

        await this.loader.present();
        return new Promise((resolve, reject) => {

    return this.httpService.get('user/other_profile_show/' + id)
            .subscribe(
                (data: any) => {
                    console.log(data);
                    this.loader.dismiss();
                    if (data['status'] === 1) {
                        resolve(data.data);
                    }
                },
                (err: any) => {
                    this.loader.dismiss();

                    throw(err);
                    reject(err);
                });
         });
    }

    async updateProfilePicture(profileData) {

        return this.httpService.post('user/profile_picture_update', profileData)
            .subscribe(
                (data: any) => {
                    // TODO check if loadercal storage is available only then set token.Not sure if such a case can occur in mobile.
                    // let test = data;
                    console.log(data);
                    this.events.publish('user:updateProfile');
                },
                (err: any) => {
                    console.log(err);
                    // this.helperService.showToast(errors.message, 2000, 'bottom');
                });
    }

    async universityProfileItem(endPoint) {

        return new Promise((resolve, reject) => {
            this.httpService.get(endPoint)
                .subscribe(
                    (data: any) => {
                        resolve(data);
                    },
                    (err: any) => {
                        this.loader.dismiss();
                        console.log(err);
                        throw(err);
                        // this.helperService.showToast(errors.message, 2000, 'bottom');
                    });
        });
    }

    async universityProfile(universityId) {
        this.loader = await this.loading.create({
            message: 'Please wait...'
        });
        await this.loader.present();
        const url = 'university/show/' + universityId;
        return new Promise((resolve, reject) => {
            this.httpService.get(url)
                .subscribe(
                    (data: any) => {
                        this.loader.dismiss();
                        resolve(data);
                    },
                    (err: any) => {
                        this.loader.dismiss();
                        console.log(err);
                        throw(err);
                        // this.helperService.showToast(errors.message, 2000, 'bottom');
                    });
        });
    }

    async logout(deepLinkPath?) {
        this.loader = await this.loading.create({
            message: 'Please wait...'
        });
        await this.loader.present();
        this.httpService.post('auth/logout', {}).subscribe(
            data => {
                localStorage.removeItem('campus-app-' + this.ENV_STRING + '-token');
                localStorage.removeItem('campus-app-logged-in-user');
                localStorage.removeItem('campus-app-logged-in-user-id');
                localStorage.removeItem('campus-app-following-university-id');
                this.navCtrl.navigateRoot('/welcome');
                this.helperService.showToast('Logged out successfully.', 2000, 'bottom');
                this.loader.dismiss();
            },
            err => {
                this.loader.dismiss();
                this.helperService.showToast('Logout failed.', 2000, 'bottom');
            },
        );
    }

}

