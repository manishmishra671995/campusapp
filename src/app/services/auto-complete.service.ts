import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';


import {Country} from '../model/country.model';
import {ENV} from '../environment';
import {Storage} from '@ionic/storage';

@Injectable({
    providedIn: 'root'
})

export class AutoCompleteService {
    labelAttribute = 'name';
    API_URL: any = ENV.ENVIRONMENT[ENV.CURRENT_ENV].API_URL;
    ENV_STRING: string = ENV.CURRENT_ENV;

    private countries: Country[] = [];

    constructor(
        private http: HttpClient,
        private storage: Storage
    ) {

        this.storage.get('getProfile').then((val) => {
            this.countries = val.countries;
        });

    }

    getResults(keyword: string): Observable<any[]> {
        let url = 'user/profile_create';
        let headers;
        let params;
        // if(data) {
        //   params = new HttpParams().set(data);
        // }
        if (localStorage.getItem('campus-app-' + this.ENV_STRING + '-token')) {
            headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('campus-app-' + this.ENV_STRING + '-token'));
        }
        console.log('keyword', keyword);
        let observable: Observable<any>;

        observable = of(this.countries);

        return observable.pipe(
            map(
                (result) => {
                    return result.filter(
                        (item) => {
                            console.log(item.name);
                            return item.name.toLowerCase().startsWith(
                                keyword.toLowerCase()
                            );
                        }
                    );
                }
            )
        );

        /* if (this.countries.length === 0) {
             observable = this.http.get(url.indexOf(this.API_URL) > -1 ? url : this.API_URL + url,
                 {headers}
             ).pipe(
                 map(
                     (results: Country[]) => {
                         if (results) {
                             console.log(results);
                             this.countries = results['data']['countries'];
                         }
                         console.log(this.countries);
                         return this.countries;
                     }
                 )
             );
         } else {
             observable = of(this.countries);
         }

         return observable.pipe(
             map(
                 (result) => {
                     return result.filter(
                         (item) => {
                             console.log(item.name);
                             return item.name.toLowerCase().startsWith(
                                 keyword.toLowerCase()
                             );
                         }
                     );
                 }
             )
         );*/
    }

}
