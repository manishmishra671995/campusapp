import { TestBed } from '@angular/core/testing';

import { StateAutoCompleteService } from './state-auto-complete.service';

describe('StateAutoCompleteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StateAutoCompleteService = TestBed.get(StateAutoCompleteService);
    expect(service).toBeTruthy();
  });
});
