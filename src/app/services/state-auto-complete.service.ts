import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';


import {Country} from '../model/country.model';
import {ENV} from '../environment';
import {Storage} from '@ionic/storage';

@Injectable({
    providedIn: 'root'
})

export class StateAutoCompleteService {
    labelAttribute = 'name';
    API_URL: any = ENV.ENVIRONMENT[ENV.CURRENT_ENV].API_URL;
    ENV_STRING: string = ENV.CURRENT_ENV;

    private state: Country[] = [];
    countryId: any;

    constructor(
        private http: HttpClient,
        private storage: Storage
    ) {
    }

    countryID(id) {
        this.state = [];
        this.countryId = id;
        console.log('this.countryId', this.countryId);

    }

    getResults(keyword: string): Observable<any[]> {
        console.log('this.countryId', this.countryId);

        let url = 'user/get_states/' + this.countryId;
        console.log('url', url);
        let headers;
        let params;
        // if(data) {
        //   params = new HttpParams().set(data);
        // }
        if (localStorage.getItem('campus-app-' + this.ENV_STRING + '-token')) {
            headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('campus-app-' + this.ENV_STRING + '-token'));
        }
        console.log('keyword', keyword);
        let observable: Observable<any>;

        if (this.state.length === 0) {
            observable = this.http.get(url.indexOf(this.API_URL) > -1 ? url : this.API_URL + url,
                {headers}
            ).pipe(
                map(
                    (results: Country[]) => {
                        if (results) {
                            console.log(results);
                            this.state = results['data'];
                        }
                        console.log(this.state);
                        return this.state;
                    }
                )
            );
        } else {
            observable = of(this.state);
        }

        return observable.pipe(
            map(
                (result) => {
                    return result.filter(
                        (item) => {
                            console.log(item.name);
                            return item.name.toLowerCase().startsWith(
                                keyword.toLowerCase()
                            );
                        }
                    );
                }
            )
        );

    }

}
