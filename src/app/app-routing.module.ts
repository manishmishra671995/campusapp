import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CheckTutorial} from './providers/check-tutorial.service';
import {ENV} from './environment';

let ENV_STRING = ENV.CURRENT_ENV;

let defaultPage = 'welcome';
if (localStorage.getItem('campus-app-' + ENV_STRING + '-token')) {
    defaultPage = 'adduniversity';
}


const routes: Routes = [
    {
        path: '',
        redirectTo: defaultPage,
        pathMatch: 'full'
    },
    {
        path: 'welcome',
        loadChildren: './pages/welcome/welcome.module#WelcomePageModule'
    },
    {
        path: 'profile',
        loadChildren: () => import('./pages/profile/profile.module').then(m => m.ProfilePageModule)
    },
    {
        path: 'account',
        loadChildren: () => import('./pages/account/account.module').then(m => m.AccountModule)
    },
    {
        path: 'support',
        loadChildren: () => import('./pages/support/support.module').then(m => m.SupportModule)
    },
    {
        path: 'login',
        loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule)
    },
    {
        path: 'signup',
        loadChildren: () => import('./pages/signup/signup.module').then(m => m.SignUpModule)
    },
    {
        path: 'app',
        loadChildren: () => import('./pages/tabs-page/tabs-page.module').then(m => m.TabsModule)
    },
    {
        path: 'tutorial',
        loadChildren: () => import('./pages/tutorial/tutorial.module').then(m => m.TutorialModule),
        canLoad: [CheckTutorial]
    },
    {
        path: 'createaccount1',
        loadChildren: () => import('./pages/createaccount1/createaccount1.module').then(m => m.Createaccount1PageModule)
    },
    {
        path: 'createaccount2',
        loadChildren: () => import('./pages/createaccount2/createaccount2.module').then(m => m.Createaccount2PageModule)
    },
    {
        path: 'createpassword',
        loadChildren: () => import('./pages/createpassword/createpassword.module').then(m => m.CreatepasswordPageModule)
    },
    {
        path: 'forgetpassword',
        loadChildren: () => import('./pages/forgetpassword/forgetpassword.module').then(m => m.ForgetpasswordPageModule)
    },
    {
        path: 'home-tabs',
        loadChildren: () => import('./pages/home-tabs/home-tabs.module').then(m => m.HomeTabsPageModule)
    },
    {
        path: 'adduniversity',
        loadChildren: () => import('./pages/adduniversity/adduniversity.module').then(m => m.AdduniversityPageModule)
    },
    {
        path: 'homesearch',
        loadChildren: () => import('./pages/homesearch/homesearch.module').then(m => m.HomesearchPageModule)
    },
    {
        path: 'universitylist/:id',
        loadChildren: () => import('./pages/universitylist/universitylist.module').then(m => m.UniversitylistPageModule)
    },
    {
        path: 'universitydetail/:id',
        loadChildren: () => import('./pages/universitydetail/universitydetail.module').then(m => m.UniversitydetailPageModule)
    },
    {
        path: 'application/:id/:roomid',
        loadChildren: () => import('./pages/application/application.module').then(m => m.ApplicationPageModule)
    },
    {
        path: 'notification',
        loadChildren: () => import('./pages/notification/notification.module').then(m => m.NotificationPageModule)
    },
    {
        path: 'profileother',
        loadChildren: () => import('./pages/profileother/profileother.module').then(m => m.ProfileotherPageModule)
    },
    {
        path: 'apartmentdetail/:id',
        loadChildren: () => import('./pages/apartmentdetail/apartmentdetail.module').then(m => m.ApartmentdetailPageModule)
    },
    {
        path: 'apartmentdetail/:id/:application_id/:roomid',
        loadChildren: () => import('./pages/apartmentdetail/apartmentdetail.module').then(m => m.ApartmentdetailPageModule)
    },
    {
        path: 'friends',
        loadChildren: () => import('./pages/friends/friends.module').then(m => m.FriendsPageModule)
    },
    {
        path: 'mutualfriends/:id',
        loadChildren: () => import('./pages/mutualfriends/mutualfriends.module').then(m => m.MutualfriendsPageModule)
    },
    {
        path: 'mutualschool/:id',
        loadChildren: () => import('./pages/mutualschool/mutualschool.module').then(m => m.MutualschoolPageModule)
    },
    {
        path: 'nearby',
        loadChildren: () => import('./pages/nearby/nearby.module').then(m => m.NearbyPageModule)
    },
    {
        path: 'review',
        loadChildren: () => import('./pages/review/review.module').then(m => m.ReviewPageModule)
    },
    {
        path: 'roomatesearch/:type/:id',
        loadChildren: () => import('./pages/roomatesearch/roomatesearch.module').then(m => m.RoomatesearchPageModule)
    },
    {
        path: 'school',
        loadChildren: () => import('./pages/school/school.module').then(m => m.SchoolPageModule)
    },
    {
        path: 'university-profile/:university_item/:id/:universityId',
        loadChildren: () => import('./pages/university-profile/university-profile.module').then(m => m.UniversityProfilePageModule)
    },
    {
        path: 'otpverify',
        loadChildren: () => import('./pages/otpverify/otpverify.module').then(m => m.OtpverifyPageModule)

    },
    {
        path: 'universityreview/:type/:id',
        loadChildren: () => import('./pages/universityreview/universityreview.module').then(m => m.UniversityreviewPageModule)
    },
    {
        path: 'interest',
        loadChildren: () => import('./pages/interest/interest.module').then(m => m.InterestPageModule)
    },
    {
        path: 'chat/:id',
        loadChildren: () => import('./pages/chat/chat.module').then(m => m.ChatPageModule)
    },
    {
        path: 'test',
        loadChildren: () => import('./pages/test/test.module').then(m => m.TestPageModule)
    },
  {

    path: 'friend-profile/:id',
    loadChildren: () => import('./pages/friend-profile/friend-profile.module').then( m => m.FriendProfilePageModule)
  },
  {
    path: 'chatlist',
    loadChildren: () => import('./pages/chatlist/chatlist.module').then( m => m.ChatlistPageModule)
  },
  {
    path: 'addnewusercard/:id',
    loadChildren: () => import('./pages/addnewusercard/addnewusercard.module').then( m => m.AddnewusercardPageModule)
  }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}


