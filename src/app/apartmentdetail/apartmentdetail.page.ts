import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'apartmentdetail',
  templateUrl: './apartmentdetail.page.html',
  styleUrls: ['./apartmentdetail.page.scss'],
})
export class ApartmentdetailPage implements OnInit {
  id: any;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
  	this.id = this.route.snapshot.paramMap.get('id');
  	console.log(this.id);
  	console.log('test');
  }

}
