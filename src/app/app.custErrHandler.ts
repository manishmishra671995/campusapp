import { Injectable, Injector, ErrorHandler } from '@angular/core';
import { HelperService } from './services/index';
import { NavController } from '@ionic/angular';
@Injectable()
export class CustErrorHandler implements ErrorHandler {
  constructor(private injector: Injector,
    private helperService: HelperService,
    private navCtrl: NavController) { 
  }

  handleError(error) {
  console.log('error', error)
    let DefaultMessage = 'Something went wrong! Please try again.';
    if(error.status) {
      if(error.status === 500) {
        this.presentToast(DefaultMessage);
      } else if(error.error.status == -1) {
        this.presentToast('Session Expired. Please login to continue.');
        this.navCtrl.navigateRoot('/welcome');
      } else if(error.status === 422) {
        let errors = error.error;
        if(errors.data && errors.data[0]) {
          this.presentToast(errors.data[0]);
        } else {
          this.presentToast(DefaultMessage);
        }
      }
      else {
        let errors = error.error;
        if(errors.message) {
          this.presentToast(errors.message);
        } else {
          this.presentToast(DefaultMessage);
        }
      }
    } else {
      //this.presentToast(DefaultMessage);
    }
  }

  presentToast(msg) {
     this.helperService.showToast(msg, 2000,'bottom');
  }

}